;Wolfware Assembler
;Copyright (c) 1985-1991 Eric Tauck. All rights reserved.

;===============================================;
;                   Call_Pseu                   ;
; Calls appropriate pseudo-op routine. The      ;
; operation value (OPVAL) is used to determine  ;
; which one.                                    ;
;===============================================;

Call_Pseu Proc Near
 Mov Bx,Opval           ;operation value
 Cmp Bx,Psop_Num        ;check if out of range
 Jae Psoerr             ;jump if so
 Shl Bx                 ;offset into table
 Add Bx,Offset Psotable ;offset into segment
 Call Word [Bx]         ;call routine
 Ret

;----- psuedo-op value out of range error

Psoerr Mov Ax,0811h     ;error 17
 Call Error             ;error routine
 Ret

;----- table of psuedo op routines

Psop_Num Dw 42          ;number of pseudo-ops

Psotable Label Word
 Dw Offset Pproc        ;PROC, beginning of procedure
 Dw Offset Pendp        ;ENDP, end of procedure
 Dw Offset Org          ;ORG, origin
 Dw Offset Build_Db     ;DB, declare bytes
 Dw Offset Build_Dw     ;DW, declare word
 Dw Offset Build_Ds     ;DS, declare storage
 Dw Offset Equ          ;EQU, equates
 Dw Offset Pwidth       ;LINESIZE, output width
 Dw Offset Liston       ;LIST+, list on
 Dw Offset Listoff      ;LIST-, list off
 Dw Offset Include      ;INCLUDE, include file
 Dw Offset Label        ;LABEL, label definition
 Dw Offset Page         ;PAGE, begin new page
 Dw Offset Pagesize     ;PAGESIZE, define page size
 Dw Offset Pageon       ;PAGE+, auto paging on
 Dw Offset Pageoff      ;PAGE-, auto paging off
 Dw Offset Title        ;TITLE, define list title
 Dw Offset Subtitle     ;SUBTITLE, define list subtitle
 Dw Offset Errormax     ;ERRORMAX, set max allowed errors
 Dw Offset Pmacro       ;MACRO, start of macro definition
 Dw Offset Pendm        ;ENDM, end of macro definition
 Dw Offset Init_Mac     ;<dummy>, macro initialization code
 Dw Offset Pif          ;IF, conditional "if"
 Dw Offset Pelseif      ;ELSEIF, conditional "else-if"
 Dw Offset Pelse        ;ELSE, conditional "else"
 Dw Offset Pendif       ;ENDIF, end of conditional statement
 Dw Offset Pnextif      ;NEXTIF, conditional "if", same level
 Dw Offset Pexpon       ;EXPAND+, macro expansion on
 Dw Offset Pexpoff      ;EXPAND-, macro expansion off
 Dw Offset Pjmpon       ;JUMP+, short jump detection on
 Dw Offset Pjmpoff      ;JUMP-, short jump detection off
 Dw Offset Puseon       ;UNUSED+, flag unused symbols on
 Dw Offset Puseoff      ;UNUSED-, flag unused symbols off
 Dw Offset Psymon       ;SYMTAB+, symbol table dump on
 Dw Offset Psymoff      ;SYMTAB-, symbol table dump off
 Dw Offset Pallon       ;FLAGALL+, flag all occurences on
 Dw Offset Palloff      ;FLAGALL-, flag all occurences off
 Dw Offset Perror       ;ERROR, user error
 Dw Offset Presetc      ;RESETC, reset counter
 Dw Offset Pifn         ;IFN, conditional "if-not"
 Dw Offset Penter       ;ENTER, 286 instruction
 Dw Offset Pimul        ;IMUL, 286 extended instruction
 Endp                   ;Call_Pseu

;===============================================;
;                      Equ                      ;
; Handles equates (EQU) psuedo op. Equates a    ;
; value to a symbol.                            ;
;===============================================;

Equ Proc Near
 Test Line_Stat,Label_Flag ;make sure there is label
 Jz Equerror1           ;jump if not, error
 Cmp Pass,2             ;check if pass two
 Je Equchk              ;jump if so

;----- pass one, set value if necessary

 Mov Ax, Dval           ;value
 Mov Di,Last_Dat        ;last symbol data

 Push Ds
 Mov Ds,Sym_Seg         ;symbol table segment
 Test Word [Di+2], True_Const ;check if adequately defined before
 Jnz Noequchk           ;jump if so, do not redefine
 Mov [Di],Ax            ;set value

Noequchk
 Pop Ds
 Ret

;----- pass two, check for illegal reference

Equchk Mov Di,Last_Dat  ;last symbol data
 Push Ds
 Mov Ds,Sym_Seg         ;symbol table segment
 Mov Ax,[Di]            ;get previous value
 Pop Ds

 Cmp Ax,Dval            ;compare original and present values
 Jne Forerror           ;jump if so
 Ret

;----- no lable error

Equerror1 Mov Ax,0014h  ;error 20
 Call Error             ;error routine
 Ret

;----- illegal reference in EQU

Forerror  Mov Bx,Ax     ;original value
 Mov Ax,0822h           ;error 34
 Call Error             ;error routine
 Ret
 Endp                   ;Equ

;===============================================;
;                       Org                     ;
; Handles origin (ORG) pseudo-op. Resets the    ;
; location counter.                             ;
;===============================================;

Org Proc Near
 Mov Ax,Dval            ;destination value
 Test Dtype, Signed     ;test if signed
 Jz Orgrele             ;jump if not

;--- relative origin

 Add Ax, Loctr          ;make it a relative origin
 Mov Loctr,Ax           ;put into location counter
 Ret

;--- non-relative origin

Orgrele
 Mov Loctr,Ax           ;put into location counter
 Mov Phase_Off, 0       ;reset phase offset
 Ret
 Endp                   ;Org

;===============================================;
;                   Build_Db                    ;
; Handles declare byte(s) (DB) pseudo-op.       ;
; Declares any combination of bytes or strings  ;
; of data.                                      ;
;===============================================;

Build_Db Proc Near
 Mov Di,Obj_Buffer      ;object buffer

;----- loop as long as there is line remaining

Nextdb Cmp Line_Rem,0   ;check if no line remaining
 Je Dbdone              ;jump if so, finished

 Push Di
 Call Eval_Oprnd        ;get and evaluate next operand
 Pop Di

 Test Ax,Immed          ;check if immediate data
 Jnz Decbyt
 Test Ax,String         ;check if string
 Jnz Decstr
 Test Ax,None Or Undef  ;check if none
 Jnz Nextdb
 Jmps Dberror3          ;invalid type

;----- immediate data

Decbyt Test Cx,S8bit    ;check if eight bit
 Jz Decwor              ;jump if too big
 Mov Al,Bl
 Stosb                  ;store data
 Jmps Nextdb            ;loop for next field

Decwor Mov Ax,Bx        ;declare entire word
 Stosw                  ;store data
 Jmps Nextdb            ;loop for next field

;----- string data

Decstr Mov Si,Str_Buff  ;source
 Store_Str              ;move string
 Jmps Nextdb            ;loop for next field

;----- end of DB

Dbdone Sub Di,Obj_Buffer ;DI has bytes written
 Mov Obj_Length,Di      ;save number of bytes
 Add Loctr,Di           ;update location counter
 Add Counter_Num,Di     ;byte counter
 Add Obj_Count,Di       ;total byte count
 Mov Cx,Di
 Call New_Chksum        ;calculate checksum
 Ret

;----- illegal operand

Dberror3 
 Mov Ax,0178h           ;error 120
 Call Error             ;error routine
 Jmps Nextdb            ;loop back for next operand
 Endp                   ;Build_Db

;===============================================;
;                    Build_Ds                   ;
; Handles declare storage (DS) pseudo-op.       ;
; Declares any number of bytes of identically   ;
; initialized data.                             ;
;===============================================;

Build_Ds Proc Near

;----- update location counter

 Mov Ax,Dval      ;bytes to write
 Or Ax,Ax               ;check if zero
 Jz Dsdone              ;jump if zero bytes declared

 Add Loctr,Ax           ;update location counter
 Add Counter_Num,Ax     ;byte counter
 Add Obj_Count,Ax       ;total byte count

 Cmp Pass,2             ;which pass
 Jne Dsdone             ;jump if not pass two

 Mov Cx,String_Siz      ;bytes to write at one time
 Mov Di,Obj_Buffer      ;object buffer location

;----- check output code

 Sub Al,Al              ;default output value, zero
 Test Stype,Immed       ;check value other than default
 Jz Deffil              ;jump if default output code
 Test Ssize,S8bit       ;make sure it's byte
 Jz Dserror             ;jump if not
 Mov Al,Byte Sval       ;use the indicated source value

;----- fill entire buffer with byte

Deffil Rep
 Stosb

;----- update checksum

 Sub Ah,Ah              ;holds temporary checksum
 Mov Cx,Dval            ;total bytes

Dschkl
 Add Ah,Al              ;add byte to checksum
 Loop Dschkl            ;loop for each byte

 Add Counter_Chk,Ah     ;counter checksum
 Add Program_Chk,Ah     ;program checksum

;----- prepare for sending code to file

 Mov Ax,Dval            ;bytes to write
 Mov Cx,String_Siz      ;max bytes to write at one time
 Sub Dx,Dx
 Div Ax,Cx              ;number of whole buffers to write
 Push Dx                ;save remainder
 Or Ax,Ax               ;check if zero
 Jz Smachk              ;jump if zero, less than a buffer
 Mov Cx,Ax

;----- write full buffer fulls

Bigstor Mov Obj_Length,String_Siz
                        ;set bytes to write
 Push Cx
 Call Obj_Send          ;write buffer
 Pop Cx
 Loop Bigstor           ;loop for all buffer fulls
 Mov Obj_Length,0       ;set bytes to write at zero

;----- write remainder

Smachk Pop Ax           ;restore remainder
 Or Ax,Ax               ;check if zero
 Jz Dsdone              ;jump if zero, all done
 Mov Obj_Length,Ax      ;set bytes to write
 Call Obj_Send          ;write
 Mov Obj_Length,0       ;set bytes to write at zero
Dsdone Ret

;----- data to long for output byte

Dserror Mov Bx,Sval     ;value
 Mov Ax,0829h           ;error 41
 Call Error             ;error routine
 Mov Al,Byte Sval       ;output code, source value
 Jmps Deffil            ;jump back using only low byte
 Endp                   ;Build_Ds

;===============================================;
;                   Build_Dw                    ;
; Handles the declare word (DW) psuedo op.      ;
; Declares any number of words of data.         ;
;===============================================;

Build_Dw Proc Near
 Mov Di,Obj_Buffer      ;object buffer

;----- loop as long as there are fields

Nextdw Cmp Line_Rem,0   ;check if no line remaining
 Je Dwdone              ;jump if so, finished

 Push Di
 Call Eval_Oprnd        ;get and evaluate next operand
 Pop Di

 Test Ax,None Or Undef  ;check if none
 Jnz Nextdw
 Test Ax,Immed          ;check if immediate data
 Jz Dwerror             ;jump if not

;----- immediate data

 Mov Ax,Bx              ;value
 Stosw                  ;store word
 Jmps Nextdw

;----- end of DW

Dwdone Sub Di,Obj_Buffer ;DI has bytes written
 Mov Obj_Length,Di      ;save number of bytes
 Add Loctr,Di           ;update location counter
 Add Counter_Num,Di     ;byte counter
 Add Obj_Count,Di       ;total byte count
 Mov Cx,Di
 Call New_Chksum        ;calculate checksum
 Ret

;----- illegal operand

Dwerror 
 Mov Ax,0178h           ;error 120
 Call Error             ;error routine
 Jmps Nextdw            ;loop back for next operand
 Endp                   ;Build_Dw

;===============================================;
;                     Pproc                     ;
; Handles procedure (PROC) psuedo op. Declares  ;
; the beginning of a procedure.                 ;
;===============================================;

Pproc Proc Near

;----- update PROC stack

 Mov Al,Stack_Top       ;top of stack
 Cmp Al,Max_Proc        ;compare maximum
 Je Proerror            ;jump if too many

 Sub Ah,Ah
 Shl Ax                 ;offset into stack
 Mov Bx,Stack_Base      ;stack base
 Add Bx,Ax              ;location of stack top

 Inc Stack_Top          ;new top

 Mov Ax,Dtype           ;DTYPE, type to enter
 Mov [Bx],Ax            ;enter as top value
 Ret

;----- too many nested PROC's error

Proerror Mov Ax,0220h   ;error 32
 Mov Bx,Max_Proc        ;print maximum nesting
 Call Error             ;error routine
 Ret
 Endp                   ;Pproc

;===============================================;
;                     Pendp                     ;
; Handles end procedure (ENDP) psuedo op.       ;
; Declares the end of a procedure.              ;
;===============================================;

Pendp Proc Near
 Mov Al,Stack_Top       ;stack top
 Or Al,Al               ;check if zero
 Jz Enderror            ;jump if ENDP without PROC
 Dec Al                 ;decrease top
 Mov Stack_Top,Al       ;save new top
 Ret

;----- ENDP without PROC error

Enderror Mov Ax,001eh   ;error 30
 Call Error             ;error routine
 Ret
 Endp                   ;Pendp

;===============================================;
;                    Pwidth                     ;
; Handles line size (LINESIZE) psuedo op.       ;
; Changes the list output width and sends       ;
; special printer output codes.                 ;
;===============================================;

Pwidth Proc Near
 Test Dtype, None       ;test if linesize specified
 Jnz Pwspecl            ;jump if none

 Mov Ax,Dval            ;destination value
 Mov Dx,Min_Width       ;minimum width
 Cmp Ax,Dx              ;check if below
 Jb Mwerror             ;jump if so, error
 Mov Dx,Max_Width       ;maximum width
 Cmp Ax,Dx              ;check if above
 Ja Mwerror             ;jump if so, error

Werrset
 Cmp Pass,1             ;check if pass one
 Jne Pwspecl            ;jump if not, only set on pass one

 Mov Width,Al           ;set width

;----- check for special printer code

Pwspecl Cmp Line_Rem,0  ;check if no line remaining
 Je Nowprn              ;jump if so, finished

 Call Eval_Oprnd        ;get and evaluate next operand

 Test Ax,None Or Undef  ;check if none
 Jnz Pwspecl            ;get next if so
 Test Ax,Immed          ;check if immediate data
 Jz Pwerror2            ;jump if not, error
 Test Cx,S8bit          ;check if 8 bit data
 Jz Pwerror2            ;jump if not, error

;----- printer code found

 Cmp List_Type,Prn_File ;check if printer
 Jne Pwspecl            ;jump if not
 Cmp Pass,1             ;check if pass one
 Jne Pwspecl            ;jump if not, only send on pass one

 Mov Dl,Bl              ;byte to output
 Dos_Function 5         ;printer output
 Jmps Pwspecl

Nowprn Ret

;----- width either too low or too high

Mwerror
 Mov Bx,Ax              ;value
 Mov Ax,0215h           ;error 21
 Call Error             ;error routine
 Mov Ax, Dx
 Jmps Werrset

;----- extra operand either too big or illegal

Pwerror2 
 Mov Ax,0161h           ;error 97
 Call Error             ;error routine
 Jmps Pwspecl           ;next operand
 Endp                   ;Pwidth

;===============================================;
;                     Plist                     ;
; Handles the list (LIST+,LIST-) psuedo-op.     ;
; Turns the source listing on or off.           ;
;===============================================;

;----- listing on, LIST+

Liston Proc Near
 Cmp List_Type,Nul_File ;check if no list
 Je Nopliston           ;jump if so, ignore
 Cmp Pass,2             ;which pass
 Jne Nopliston          ;don't bother on pass two
 Or Opt_Stat,List_Flag  ;turn list on

Nopliston Ret
 Endp                   ;Liston

;----- listing off, LIST-

Listoff Proc Near
 Cmp List_Type,Nul_File ;check if no list
 Je Nomls               ;jump if so, ignore
 Cmp Pass,2             ;which pass
 Jne Nomls              ;don't bother if not pass two
 And Opt_Stat,Not List_Flag ;list off
Nomls Ret
 Endp                   ;Listoff

;===============================================;
;                    Include                    ;
; Handles the include (INCLUDE) psuedo-op.      ;
; Allows another file to be inserted as source  ;
; code.                                         ;
;===============================================;

Include Proc Near
 Mov Di,Offset Def_Iname ;default include name
 Mov Si,Str_Buff        ;name location
 Call Push_Source       ;open and push source file
 Ret

;----- default include name

Def_Iname Db 'Nul.Asm',0
 Endp                   ;Include

;===============================================;
;                  Push_Source                  ;
; Push a source file input level (for include). ;
; Presently only one push is supported. SI must ;
; point to the file name to use and DI must     ;
; point to the default name.                    ;
;===============================================;

Push_Source Proc Near
 Test Mac_Stat, Mac_Flag ;test if in macro
 Jnz Incmacerr          ;jump if so, error
 Test Input_Stat,Inc_Flag ;check include flag
 Jnz Incalready         ;jump if set, too deep

;----- construct name

 Push Si
 Push Di
 Call Reset_Nam         ;reset default name
 Mov Si,Src_Name        ;source name
 Call Reextract         ;default settings (path)

 Pop Si
 Call Reextract         ;default settings (name and ext.)

 Pop Si
 Mov Di,Inc_Name        ;include
 Call Format_Name       ;format

;----- open file

 Mov Dx,Inc_Name        ;file name
 Call Open_File         ;open file
 Jc Pierroro            ;jump if file doesn't open
 Push Ax                ;save file handle

;----- save all original source info

 Mov Di,Inctor          ;include storage location
 Mov Ax,Src_Handle      ;handle
 Stosw
 Mov Ax,Src_End         ;buffer end
 Stosw
 Mov Ax,Src_Seg         ;segment
 Stosw
 Mov Al,Input_Stat      ;source status
 Stosb
 Mov Ax,Src_Point       ;source pointer
 Stosw
 Mov Ax,Line_Num        ;present line number
 Stosw
 Mov Al,Opt_Stat        ;list status
 Stosb

;----- new information

 Pop Ax
 Mov Src_Handle,Ax      ;new handle
 Mov Ax,Inc_Seg
 Mov Src_Seg,Ax         ;segment

 Or Input_Stat,Inc_Flag ;include on
 Or Line_Stat,Inc_Line  ;set include line
 Or Input_Stat,Src_First Or Src_Flag ;first read and source flag on
 And Input_Stat,Not (Src_Whole Or Fil_Done) ;whole file and done flag off

 Call Read              ;read a buffer full
 Mov Line_Num,0         ;reset line numbering
 Ret 

;----- inside of macro, not allowed

Incmacerr
 Mov Ax, 000dh          ;error 13
 Call Error             ;error routine
 Ret

;----- attempt to include while already including

Incalready
 Mov Ax,0024h           ;error 36
 Call Error             ;error routine
 Ret

;----- could not open include file error

Pierroro
 Mov Ax,1018h           ;error 24
 Call Error             ;error routine
 Ret
 Endp                   ;Push_Source

;===============================================;
;                   Pop_Source                  ;
; Pop a source file input level (for include).  ;
;===============================================;

Pop_Source Proc Near
 Mov Bx,Src_Handle      ;file handle
 Call Close_File        ;close file

;----- restore original source file

 Mov Si,Inctor
 Lodsw
 Mov Src_Handle,Ax      ;source handle
 Lodsw
 Mov Src_End,Ax         ;buffer end
 Lodsw
 Mov Src_Seg,Ax         ;segment
 Lodsb
 Mov Input_Stat,Al      ;source status
 Lodsw
 Mov Src_Point,Ax       ;save source buffer pointer
 Lodsw
 Mov Line_Num,Ax        ;old line number
 Lodsb                  ;option status
 And Al,Inc_Preserve    ;mask saved bits
 And Opt_Stat,Not Inc_Preserve ;clear relevant bits
 Or Opt_Stat,Al         ;set bits
 Ret
 Endp                   ;Pop_Source

;===============================================;
;                     Label                     ;
; Handles declare label (LABEL) pseudo-op.      ;
; Declares a particular label type. Symbol is   ;
; actually declared in pass zero, this routine  ;
; just checks for a missing label.              ;
;===============================================;

Label Proc Near
 Test Line_Stat,Label_Flag ;make sure there is label
 Jz Labelerr            ;jump if false
 Ret

;----- no label error

Labelerr Mov Ax,001bh   ;error 27
 Call Error             ;error routine
 Ret
 Endp                   ;Label

;===============================================;
;                     Page                      ;
; Handles page (PAGE) pseudo-op. Starts a new   ;
; list page.                                    ;
;===============================================;

Page Proc Near
 Cmp Pass,2             ;which pass
 Jne Nnpag              ;don't bother if not pass two
 Test Opt_Stat,List_Flag ;is list on?
 Jz Nnpag               ;no page if not on

;----- check for subtitle first operand

 Test Dtype,String      ;test if string
 Jz Ppagckpn            ;jump if not
 Inc Page_Num           ;next page
 Jmps Pagsubs           ;set subtitle

;----- check for page number first operand

Ppagckpn Test Dtype,Immed ;see if specified
 Jz Nopset              ;jump if not
 Mov Ax,Dval            ;get page number
 Mov Page_Num,Ax        ;set it

;----- check for subtitle second operand, with page

 Test Stype,String      ;check if extra string
 Jnz Pagsubs            ;jump if so
 Jmps Nopagst

;----- check for subtitle second operand, no page

Nopset Inc Page_Num     ;next page
 Test Stype,String      ;test if string
 Jz Nopagst

;----- save subtitle

Pagsubs Mov Di,Subt_Buff ;subtitle location
 Mov Si,Str_Buff        ;string location
 Copy_Str               ;move string

Nopagst 
 Test Opt_Stat, List_Flag ;test if listing
 Jz Nnpag               ;jump if not
 Mov Page_Line,0        ;reset page line number
 Call Header            ;header
Nnpag Ret
 Endp                   ;Page

;===============================================;
;                   Pagesize                    ;
; Handles page size (PAGESIZE) pseudo-op.       ;
; Sets the number of lines per page.            ;
;===============================================;

Pagesize Proc Near

;----- set page length

 Test Dtype,None        ;check if page length specified
 Jnz Pgchklin           ;jump if not

 Mov Ax, Dval           ;size value
 Mov Dx, Min_Page       ;minimum page size
 Cmp Ax,Dx              ;check if too small
 Jb Pagerr              ;jump to error if is
 Mov Dx, Max_Page       ;maximum page size
 Cmp Ax,Dx              ;check if too big
 Ja Pagerr              ;jump to error if is

Ppagsec Cmp Pass,1      ;check if pass one
 Jne Pgchklin           ;jump if not, only set on pass one
 Mov Page_Size,Al       ;set new size

;----- set page width

Pgchklin Test Stype,None ;check if line size specified
 Jnz Nnpagesi           ;jump if not

 Mov Ax,Sval            ;source value
 Mov Dx,Min_Width       ;minimum width
 Cmp Ax,Dx              ;check if below
 Jb Pmwerror            ;jump if so, error
 Mov Dx,Max_Width       ;maximum width
 Cmp Ax,Dx              ;check if above
 Ja Pmwerror            ;jump if so, error

Ppagslec Cmp Pass,1     ;check if pass one
 Jne Nnpagesi           ;jump if not, only set on pass one
 Mov Width,Al           ;set width
Nnpagesi Ret

;----- page size out of range error

Pagerr
 Mov Bx,Ax              ;value
 Mov Ax,0226h           ;error 38
 Call Error             ;error routine
 Mov Ax,Dx              ;set to limit
 Jmps Ppagsec

;----- line size out of range error

Pmwerror
 Mov Bx,Ax              ;value
 Mov Ax,0215h           ;error 21
 Call Error             ;error routine
 Mov Ax,Dx              ;set to limit
 Jmps Ppagslec
 Ret
 Endp                   ;Pagesize

;===============================================;
;                  Page On/Off                  ;
; Handles page on and off (PAGE+ and PAGE-)     ;
; pseudo-ops.  Turns the automatic paging on    ;
; (PAGE+) or off (PAGE-).                       ;
;===============================================;

;----- page on

Pageon Proc Near
 Cmp Pass,2             ;check if pass two
 Jne Npon               ;skip if not
 Or Opt_Stat,Page_Flag  ;set page on
 Test Dtype,None        ;check if no operands
 Jnz Npon               ;no new page if none
 Call Page              ;start a page
Npon Ret
 Endp                   ;Pageon

;----- page off

Pageoff Proc Near
 Cmp Pass,2             ;check if pass two
 Jne Npoff              ;skip if not
 And Opt_Stat,Not Page_Flag ;set page off
Npoff Ret
 Endp                   ;Pageoff

;===============================================;
;                     Title                     ;
; Handles title (TITLE) pseudo-op. Sets the     ;
; title for all pages.                          ;
;===============================================;

Title Proc Near
 Call Eval_Oprnd        ;look up operand
 Test Ax,String         ;test if string
 Jz Titerr              ;jump if not

;----- save title

 Mov Di,Title_Buff      ;title location
 Mov Si,Str_Buff        ;string location
 Copy_Str               ;move string

;----- check for subtitle

 Call Eval_Oprnd        ;look up operand
 Test Ax,None           ;check if operand
 Jnz Titdon             ;jump if none
 Test Ax,String         ;test if string
 Jz Titerr              ;jump if not

 Mov Di,Subt_Buff       ;title location
 Mov Si,Str_Buff        ;string location
 Copy_Str               ;move string
Titdon Ret

;----- error in operands

Titerr 
 Test Ax,Undef          ;check if operand undefined
 Jnz Titdon             ;jump if so, forget error message
 Mov Ax,0016h           ;error 22
 Call Error             ;error routine
 Ret
 Endp                   ;Title

;===============================================;
;                   Subtitle                    ;
; Handles subtitle (SUBTITLE) pseudo-op. Sets   ;
; the subtitle for all following pages.         ;
;===============================================;

Subtitle Proc Near
 Cmp Pass,2             ;check if pass two
 Jne Nosubtitle         ;jump if not, no effect

;----- save subtitle

 Mov Di,Subt_Buff       ;subtitle location
 Mov Si,Str_Buff        ;string location
 Copy_Str               ;move string
Nosubtitle Ret
 Endp                   ;Subtitle

;===============================================;
;                    Errormax                   ;
; Handles maximum errors (ERRORMAX) pseudo-op.  ;
; Sets the maximum number of allowed errors.    ;
;===============================================;

Errormax Proc Near
 Cmp Pass,1             ;check if pass one
 Jne Nosemax            ;jump if not, don't bother
 Mov Ax,Dval
 Mov Error_Max,Ax       ;set to number
Nosemax Ret
 Endp                   ;Errormax

