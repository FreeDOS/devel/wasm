;Wolfware Assembler
;Copyright (c) 1985-1991 Eric Tauck. All rights reserved.

;===============================================;
;              Reserved Symbols                 ;
;===============================================;

;----- special operands

Specdir Label Byte
 Db 4,'NEAR'
 Db 3,'FAR'
 Db 6,'OFFSET'
 Db 3,'NOT'
 Db 4,'BYTE'
 Db 4,'WORD'
 Db 5,'DWORD'
 Db 5,'QWORD'
 Db 5,'TBYTE'
 Db 7,'ANYSIZE'
 Db 2,'ST'
 Db 5,'VALUE'
 Db 4,'SIZE'
 Db 4,'TYPE'
 Db 3,'NEG'
 Db 0

;----- constants

Constdir Label Byte
 Db 1,'$'
 Db 4,'$LOC'
 Db 5,'$SIZE'
 Db 6,'$COUNT'
 Db 4,'$SUM'
 Db 7,'$CHKSUM'
 Db 8,'$VERSION'
 Db 6,'$TIME1'
 Db 6,'$TIME2'
 Db 6,'$DATE1'
 Db 6,'$DATE2'
 Db 4,'$END'
 Db 0

;----- other

Hexbase Db '0123456789ABCDEF' ;numerical digits

;===============================================;
;      Wolfware Assembler Instruction Set       ;
; Operation table one. List of instructions.    ;
; Entry contains the the mnemonic and various   ;
; instruction information. Each entry must be   ;
; 16 bytes long.                                ;
;===============================================;

;----- table structure

Op_Num Dw 266           ;number of instructions
Op_Table1 Dw Offset Optable ;start of table
Op_Table2 Dw ?          ;operation table two (search table)

;Mentry_Len Equ 16      ;size of single entry
Mentry_Flg Equ 14       ;offset into single entry for flags
Mstr_Len Equ 10         ;buffered mnemonic length plus length byte

;----- format: len, mnemonic, operands, variations, data location, pre-flags
;----- sizes: byte 9 bytes byte byte word word

 Ds $ And 1             ;puts the table on an even address, declares
                        ;one byte if presently at odd location

Optable Label Byte

 Db 3,'MOV',0,0,0,0,0,0, 2, 5
 Dw Offset Op1, 0

 Db 5,'MOVSB',0,0,0,0, 0, 1
 Dw Offset Op2, 0

 Db 5,'MOVSW',0,0,0,0, 0, 1
 Dw Offset Op3, 0

 Db 4,'CALL',0,0,0,0,0, 2, 4
 Dw Offset Op4, 0

 Db 3,'RET',0,0,0,0,0,0, 2, 4
 Dw Offset Op5, Return*256

 Db 3,'REP',0,0,0,0,0,0, 0, 1
 Dw Offset Op6, 0

 Db 4,'REPE',0,0,0,0,0, 0, 1
 Dw Offset Op7, 0

 Db 4,'REPZ',0,0,0,0,0, 0, 1
 Dw Offset Op8, 0

 Db 5,'REPNE',0,0,0,0, 0, 1
 Dw Offset Op9, 0

 Db 5,'REPNZ',0,0,0,0, 0, 1
 Dw Offset Op10, 0

 Db 4,'RETF',0,0,0,0,0, 1, 2
 Dw Offset Op35, 0

 Db 4,'RETN',0,0,0,0,0, 1, 2
 Dw Offset Op37, 0

 Db 4,'PROC',0,0,0,0,0, 1, 1
 Dw Offset Op11, Proc*256+1

 Db 4,'ENDP',0,0,0,0,0, 0, 1
 Dw Offset Op12, Endp*256

 Db 4,'ENDM',0,0,0,0,0, 0, 1
 Dw Offset Op193, Endm*256

 Db 2,'DB',0,0,0,0,0,0,0, 0, 1
 Dw Offset Op13, 2

 Db 2,'DW',0,0,0,0,0,0,0, 0, 1
 Dw Offset Op14, 3

 Db 3,'CMP',0,0,0,0,0,0, 2, 3
 Dw Offset Op15, 0

 Db 5,'CMPSB',0,0,0,0, 0, 1
 Dw Offset Op16, 0

 Db 5,'CMPSW',0,0,0,0, 0, 1
 Dw Offset Op17, 0

 Db 2,'JE',0,0,0,0,0,0,0, 1, 1
 Dw Offset Op18, 0

 Db 4,'JMPS',0,0,0,0,0, 1, 1
 Dw Offset Op19, 0

 Db 3,'JMP',0,0,0,0,0,0, 2, 4
 Dw Offset Op20, Jump*256

 Db 3,'EQU',0,0,0,0,0,0, 1, 1
 Dw Offset Op21, Equate*256+5

 Db 3,'POP',0,0,0,0,0,0, 1, 3
 Dw Offset Op22, 0

 Db 4,'POPA',0,0,0,0,0, 0, 1
 Dw Offset Op229, 0

 Db 4,'POPF',0,0,0,0,0, 0, 1
 Dw Offset Op23, 0

 Db 4,'PUSH',0,0,0,0,0, 1, 5
 Dw Offset Op24, 0

 Db 5,'PUSHA',0,0,0,0, 0, 1
 Dw Offset Op228, 0

 Db 5,'PUSHF',0,0,0,0, 0, 1
 Dw Offset Op25, 0

 Db 3,'ADD',0,0,0,0,0,0, 2, 3
 Dw Offset Op26, 0

 Db 3,'ADC',0,0,0,0,0,0, 2, 3
 Dw Offset Op27, 0

 Db 3,'SUB',0,0,0,0,0,0, 2, 3
 Dw Offset Op28, 0

 Db 3,'INC',0,0,0,0,0,0, 1, 2
 Dw Offset Op29, 0

 Db 2,'IN',0,0,0,0,0,0,0, 2, 2
 Dw Offset Op30, 0

 Db 7,'INCLUDE',0,0, 1, 1
 Dw Offset Op31, 10h

 Db 4,'INSB',0,0,0,0,0, 0, 1
 Dw Offset Op230, 0

 Db 4,'INSW',0,0,0,0,0, 0, 1
 Dw Offset Op238, 0

 Db 3,'INT',0,0,0,0,0,0, 1, 1
 Dw Offset Op32, 0

 Db 4,'INT3',0,0,0,0,0, 0, 1
 Dw Offset Op120, 0

 Db 4,'INTO',0,0,0,0,0, 0, 1
 Dw Offset Op33, 0

 Db 3,'DEC',0,0,0,0,0,0, 1, 2
 Dw Offset Op34, 0

 Db 2,'JZ',0,0,0,0,0,0,0, 1, 1
 Dw Offset Op18, 0

 Db 3,'JNE',0,0,0,0,0,0, 1, 1
 Dw Offset Op36, 0

 Db 3,'JNZ',0,0,0,0,0,0, 1, 1
 Dw Offset Op36, 0

 Db 3,'JNA',0,0,0,0,0,0, 1, 1
 Dw Offset Op79, 0

 Db 4,'JNAE',0,0,0,0,0, 1, 1
 Dw Offset Op78, 0

 Db 3,'JNB',0,0,0,0,0,0, 1, 1
 Dw Offset Op38, 0

 Db 4,'JNBE',0,0,0,0,0, 1, 1
 Dw Offset Op76, 0

 Db 3,'JNC',0,0,0,0,0,0, 1, 1
 Dw Offset Op38, 0

 Db 3,'JNG',0,0,0,0,0,0, 1, 1
 Dw Offset Op83, 0

 Db 4,'JNGE',0,0,0,0,0, 1, 1
 Dw Offset Op82, 0

 Db 3,'JNL',0,0,0,0,0,0, 1, 1
 Dw Offset Op81, 0

 Db 4,'JNLE',0,0,0,0,0, 1, 1
 Dw Offset Op54, 0

 Db 3,'JNO',0,0,0,0,0,0, 1, 1
 Dw Offset Op39, 0

 Db 3,'JNP',0,0,0,0,0,0, 1, 1
 Dw Offset Op40, 0

 Db 3,'JNS',0,0,0,0,0,0, 1, 1
 Dw Offset Op41, 0

 Db 2,'OR',0,0,0,0,0,0,0, 2, 3
 Dw Offset Op42, 0

 Db 3,'ORG',0,0,0,0,0,0, 1, 1
 Dw Offset Op43, 0

 Db 5,'STOSB',0,0,0,0, 0, 1
 Dw Offset Op44, 0

 Db 5,'STOSW',0,0,0,0, 0, 1
 Dw Offset Op45, 0

 Db 3,'STC',0,0,0,0,0,0, 0, 1
 Dw Offset Op46, 0

 Db 3,'STD',0,0,0,0,0,0, 0, 1
 Dw Offset Op47, 0

 Db 3,'STI',0,0,0,0,0,0, 0, 1
 Dw Offset Op48, 0

 Db 5,'LODSB',0,0,0,0, 0, 1
 Dw Offset Op49, 0

 Db 5,'LODSW',0,0,0,0, 0, 1
 Dw Offset Op50, 0

 Db 4,'LOCK',0,0,0,0,0, 0, 1
 Dw Offset Op51, 0

 Db 4,'LOOP',0,0,0,0,0, 1, 1
 Dw Offset Op52, 0

 Db 5,'LOOPE',0,0,0,0, 1, 1
 Dw Offset Op53, 0

 Db 5,'LOOPZ',0,0,0,0, 1, 1
 Dw Offset Op53, 0

 Db 6,'LOOPNE',0,0,0, 1, 1
 Dw Offset Op55, 0

 Db 6,'LOOPNZ',0,0,0, 1, 1
 Dw Offset Op55, 0

 Db 3,'AAA',0,0,0,0,0,0, 0, 1
 Dw Offset Op57, 0

 Db 3,'AAD',0,0,0,0,0,0, 0, 1
 Dw Offset Op61, 0

 Db 3,'AAM',0,0,0,0,0,0, 0, 1
 Dw Offset Op59, 0

 Db 3,'AAS',0,0,0,0,0,0, 0, 1
 Dw Offset Op60, 0

 Db 3,'AND',0,0,0,0,0,0, 2, 3
 Dw Offset Op58, 0

 Db 4,'ARPL',0,0,0,0,0, 2, 1
 Dw Offset Op247, 0

 Db 5,'BOUND',0,0,0,0, 2, 1
 Dw Offset Op225, 0

 Db 3,'CBW',0,0,0,0,0,0, 0, 1
 Dw Offset Op62, 0

 Db 3,'CLC',0,0,0,0,0,0, 0, 1
 Dw Offset Op63, 0

 Db 3,'CLD',0,0,0,0,0,0, 0, 1
 Dw Offset Op64, 0

 Db 3,'CLI',0,0,0,0,0,0, 0, 1
 Dw Offset Op65, 0

 Db 4,'CLTS',0,0,0,0,0, 0, 1
 Dw Offset Op232, 0

 Db 3,'CMC',0,0,0,0,0,0, 0, 1
 Dw Offset Op66, 0

 Db 3,'CS:',0,0,0,0,0,0, 0, 1
 Dw Offset Op213, 0

 Db 3,'CWD',0,0,0,0,0,0, 0, 1
 Dw Offset Op67, 0

 Db 3,'DAA',0,0,0,0,0,0, 0, 1
 Dw Offset Op68, 0

 Db 3,'DAS',0,0,0,0,0,0, 0, 1
 Dw Offset Op69, 0

 Db 3,'DIV',0,0,0,0,0,0, 2, 2
 Dw Offset Op70, 0

 Db 2,'DS',0,0,0,0,0,0,0, 2, 1
 Dw Offset Op71, 4

 Db 3,'DS:',0,0,0,0,0,0, 0, 1
 Dw Offset Op214, 0

 Db 4,'ELSE',0,0,0,0,0, 0, 1
 Dw Offset Op197, 20h

 Db 6,'ELSEIF',0,0,0, 1, 1
 Dw Offset Op196, 20h

 Db 5,'ENDIF',0,0,0,0, 0, 1
 Dw Offset Op198, 20h

 Db 5,'ENTER',0,0,0,0, 2, 1
 Dw Offset Op226, 0

 Db 5,'ERROR',0,0,0,0, 1, 1
 Dw Offset Op210, 0

 Db 8,'ERRORMAX',0, 1, 1
 Dw Offset Op192, 0

 Db 3,'ES:',0,0,0,0,0,0, 0, 1
 Dw Offset Op215, 0

 Db 7,'EXPAND+',0,0, 0, 1
 Dw Offset Op200, 0

 Db 7,'EXPAND-',0,0, 0, 1
 Dw Offset Op201, 0

 Db 8,'FLAGALL+',0, 0, 1
 Dw Offset Op208, 0

 Db 8,'FLAGALL-',0, 0, 1
 Dw Offset Op209, 0

 Db 3,'HLT',0,0,0,0,0,0, 0, 1
 Dw Offset Op72, 0

 Db 4,'IDIV',0,0,0,0,0, 2, 2
 Dw Offset Op73, 0

 Db 2,'IF',0,0,0,0,0,0,0, 1, 1
 Dw Offset Op195, 20h

 Db 3,'IFN',0,0,0,0,0,0, 1, 1
 Dw Offset Op212, 20

 Db 4,'IMUL',0,0,0,0,0, 2, 4
 Dw Offset Op74, 0

 Db 4,'IRET',0,0,0,0,0, 0, 1
 Dw Offset Op75, 0

 Db 2,'JA',0,0,0,0,0,0,0, 1, 1
 Dw Offset Op76, 0

 Db 3,'JAE',0,0,0,0,0,0, 1, 1
 Dw Offset Op38, 0

 Db 2,'JB',0,0,0,0,0,0,0, 1, 1
 Dw Offset Op78, 0

 Db 3,'JBE',0,0,0,0,0,0, 1, 1
 Dw Offset Op79, 0

 Db 2,'JC',0,0,0,0,0,0,0, 1, 1
 Dw Offset Op78, 0

 Db 4,'JCXZ',0,0,0,0,0, 1, 1
 Dw Offset Op80, 0

 Db 2,'JG',0,0,0,0,0,0,0, 1, 1
 Dw Offset Op54, 0

 Db 3,'JGE',0,0,0,0,0,0, 1, 1
 Dw Offset Op81, 0

 Db 2,'JL',0,0,0,0,0,0,0, 1, 1
 Dw Offset Op82, 0

 Db 3,'JLE',0,0,0,0,0,0, 1, 1
 Dw Offset Op83, 0

 Db 2,'JO',0,0,0,0,0,0,0, 1, 1
 Dw Offset Op84, 0

 Db 2,'JP',0,0,0,0,0,0,0, 1, 1
 Dw Offset Op85, 0

 Db 3,'JPE',0,0,0,0,0,0, 1, 1
 Dw Offset Op85, 0

 Db 3,'JPO',0,0,0,0,0,0, 1, 1
 Dw Offset Op40, 0

 Db 2,'JS',0,0,0,0,0,0,0, 1, 1
 Dw Offset Op87, 0

 Db 5,'JUMP+',0,0,0,0, 0, 1
 Dw Offset Op202, 0

 Db 5,'JUMP-',0,0,0,0, 0, 1
 Dw Offset Op203, 0

 Db 5,'LABEL',0,0,0,0, 1, 2
 Dw Offset Op86, 6

 Db 4,'LAHF',0,0,0,0,0, 0, 1
 Dw Offset Op89, 0

 Db 3,'LAR',0,0,0,0,0,0, 2, 1
 Dw Offset Op248, 0

 Db 3,'LDS',0,0,0,0,0,0, 2, 1
 Dw Offset Op90, 0

 Db 3,'LEA',0,0,0,0,0,0, 2, 1
 Dw Offset Op91, 0

 Db 5,'LEAVE',0,0,0,0, 0, 1
 Dw Offset Op227, 0

 Db 3,'LES',0,0,0,0,0,0, 2, 1
 Dw Offset Op92, 0

 Db 4,'LGDT',0,0,0,0,0, 1, 1
 Dw Offset Op233, 0

 Db 4,'LIDT',0,0,0,0,0, 1, 1
 Dw Offset Op234, 0

 Db 8,'LINESIZE',0, 1, 1
 Dw Offset Op114, 0

 Db 5,'LIST+',0,0,0,0, 0, 1
 Dw Offset Op93, 0

 Db 5,'LIST-',0,0,0,0, 0, 1
 Dw Offset Op94, 0

 Db 4,'LLDT',0,0,0,0,0, 1, 1
 Dw Offset Op235, 0

 Db 4,'LMSW',0,0,0,0,0, 1, 1
 Dw Offset Op236, 0

 Db 3,'LSL',0,0,0,0,0,0, 2, 1
 Dw Offset Op249, 0

 Db 3,'LTR',0,0,0,0,0,0, 1, 1
 Dw Offset Op237, 0

 Db 5,'MACRO',0,0,0,0, 0, 1
 Dw Offset Op194, Macro*256+17h

 Db 6,'MACROC',0,0,0, 0, 1
 Dw Offset Op194, Macro*256+18h

 Db 3,'MUL',0,0,0,0,0,0, 2, 2
 Dw Offset Op95, 0

 Db 3,'NEG',0,0,0,0,0,0, 1, 1
 Dw Offset Op96, 0

 Db 6,'NEXTIF',0,0,0, 1, 1
 Dw Offset Op199, 20h

 Db 3,'NOP',0,0,0,0,0,0, 0, 1
 Dw Offset Op97, 0

 Db 3,'NOT',0,0,0,0,0,0, 1, 1
 Dw Offset Op98, 0

 Db 3,'OUT',0,0,0,0,0,0, 2, 2
 Dw Offset Op99, 0

 Db 5,'OUTSB',0,0,0,0, 0, 1
 Dw Offset Op231, 0

 Db 5,'OUTSW',0,0,0,0, 0, 1
 Dw Offset Op239, 0

 Db 4,'PAGE',0,0,0,0,0, 2, 3
 Dw Offset Op56, 0

 Db 5,'PAGE+',0,0,0,0, 2, 3
 Dw Offset Op118, 0

 Db 5,'PAGE-',0,0,0,0, 0, 1
 Dw Offset Op119, 0

 Db 8,'PAGESIZE',0, 2, 2
 Dw Offset Op88, 0

 Db 3,'RCL',0,0,0,0,0,0, 2, 3
 Dw Offset Op100, 0

 Db 3,'RCR',0,0,0,0,0,0, 2, 3
 Dw Offset Op101, 0

 Db 6,'RESETC',0,0,0, 2, 2
 Dw Offset Op211, 0

 Db 3,'ROL',0,0,0,0,0,0, 2, 3
 Dw Offset Op102, 0

 Db 3,'ROR',0,0,0,0,0,0, 2, 3
 Dw Offset Op103, 0

 Db 4,'SAHF',0,0,0,0,0, 0, 1
 Dw Offset Op104, 0

 Db 3,'SAL',0,0,0,0,0,0, 2, 3
 Dw Offset Op110, 0

 Db 3,'SAR',0,0,0,0,0,0, 2, 3
 Dw Offset Op105, 0

 Db 3,'SBB',0,0,0,0,0,0, 2, 3
 Dw Offset Op106, 0

 Db 5,'SCASB',0,0,0,0, 0, 1
 Dw Offset Op107, 0

 Db 5,'SCASW',0,0,0,0, 0, 1
 Dw Offset Op108, 0

 Db 3,'SEG',0,0,0,0,0,0, 1, 1
 Dw Offset Op109, 0

 Db 4,'SGDT',0,0,0,0,0, 1, 1
 Dw Offset Op240, 0

 Db 3,'SHL',0,0,0,0,0,0, 2, 3
 Dw Offset Op110, 0

 Db 3,'SHR',0,0,0,0,0,0, 2, 3
 Dw Offset Op111, 0

 Db 4,'SIDT',0,0,0,0,0, 1, 1
 Dw Offset Op244, 0

 Db 4,'SLDT',0,0,0,0,0, 1, 1
 Dw Offset Op245, 0

 Db 4,'SMSW',0,0,0,0,0, 1, 1
 Dw Offset Op246, 0

 Db 3,'SS:',0,0,0,0,0,0, 0, 1
 Dw Offset Op216, 0

 Db 3,'STR',0,0,0,0,0,0, 1, 1
 Dw Offset Op241, 0

 Db 8,'SUBTITLE',0, 1, 1
 Dw Offset Op191, 0

 Db 8,'SYMDUMP+',0, 0, 1
 Dw Offset Op206, 0

 Db 8,'SYMDUMP-',0, 0, 1
 Dw Offset Op207, 0

 Db 4,'TEST',0,0,0,0,0, 2, 4
 Dw Offset Op112, 0

 Db 5,'TITLE',0,0,0,0, 0, 1
 Dw Offset Op190, 0

 Db 4,'WAIT',0,0,0,0,0, 0, 1
 Dw Offset Op113, 0

 Db 7,'UNUSED+',0,0, 0, 1
 Dw Offset Op204, 0

 Db 7,'UNUSED-',0,0, 0, 1
 Dw Offset Op205, 0

 Db 4,'VERR',0,0,0,0,0, 1, 1
 Dw Offset Op242, 0

 Db 4,'VERW',0,0,0,0,0, 1, 1
 Dw Offset Op243, 0

 Db 4,'XCHG',0,0,0,0,0, 2, 4
 Dw Offset Op115, 0

 Db 4,'XLAT',0,0,0,0,0, 0, 1
 Dw Offset Op116, 0

 Db 3,'XOR',0,0,0,0,0,0, 2, 3
 Dw Offset Op117, 0

 Db 5,'F2XM1',0,0,0,0, 0, 1
 Dw Offset Op124, 0

 Db 4,'FABS',0,0,0,0,0, 0, 1
 Dw Offset Op125, 0

 Db 4,'FADD',0,0,0,0,0, 2, 8
 Dw Offset Op126, 0

 Db 5,'FIADD',0,0,0,0, 2, 4
 Dw Offset Op127, 0

 Db 5,'FADDP',0,0,0,0, 2, 2
 Dw Offset Op148, 0

 Db 4,'FCHS',0,0,0,0,0, 0, 1
 Dw Offset Op128, 0

 Db 5,'FCLEX',0,0,0,0, 0, 1
 Dw Offset Op129, 0

 Db 4,'FCOM',0,0,0,0,0, 2, 7
 Dw Offset Op130, 0

 Db 5,'FICOM',0,0,0,0, 2, 4
 Dw Offset Op131, 0

 Db 5,'FCOMP',0,0,0,0, 2, 7
 Dw Offset Op132, 0

 Db 6,'FICOMP',0,0,0, 2, 4
 Dw Offset Op133, 0

 Db 6,'FCOMPP',0,0,0, 0, 1
 Dw Offset Op134, 0

 Db 7,'FDECSTP',0,0, 0, 1
 Dw Offset Op135, 0

 Db 5,'FDISI',0,0,0,0, 0, 1
 Dw Offset Op136, 0

 Db 4,'FDIV',0,0,0,0,0, 2, 8
 Dw Offset Op137, 0

 Db 5,'FIDIV',0,0,0,0, 2, 4
 Dw Offset Op138, 0

 Db 5,'FDIVP',0,0,0,0, 2, 2
 Dw Offset Op139, 0

 Db 5,'FDIVR',0,0,0,0, 2, 8
 Dw Offset Op140, 0

 Db 6,'FIDIVR',0,0,0, 2, 4
 Dw Offset Op141, 0

 Db 6,'FDIVRP',0,0,0, 2, 2
 Dw Offset Op189, 0

 Db 4,'FSUB',0,0,0,0,0, 2, 8
 Dw Offset Op142, 0

 Db 5,'FISUB',0,0,0,0, 2, 4
 Dw Offset Op143, 0

 Db 5,'FSUBP',0,0,0,0, 2, 2
 Dw Offset Op144, 0

 Db 5,'FSUBR',0,0,0,0, 2, 8
 Dw Offset Op145, 0

 Db 6,'FISUBR',0,0,0, 2, 4
 Dw Offset Op146, 0

 Db 6,'FSUBRP',0,0,0, 2, 2
 Dw Offset Op188, 0

 Db 4,'FENI',0,0,0,0,0, 0, 1
 Dw Offset Op147, 0

 Db 5,'FFREE',0,0,0,0, 0, 2
 Dw Offset Op149, 0

 Db 7,'FINCSTP',0,0, 0, 1
 Dw Offset Op150, 0

 Db 5,'FINIT',0,0,0,0, 0, 1
 Dw Offset Op151, 0

 Db 3,'FLD',0,0,0,0,0,0, 2, 9
 Dw Offset Op121, 0

 Db 4,'FILD',0,0,0,0,0, 2, 6
 Dw Offset Op122, 0

 Db 4,'FBLD',0,0,0,0,0, 2, 2
 Dw Offset Op123, 0

 Db 4,'FLD1',0,0,0,0,0, 0, 1
 Dw Offset Op152, 0

 Db 5,'FLDCW',0,0,0,0, 1, 1
 Dw Offset Op153, 0

 Db 6,'FLDENV',0,0,0, 1, 1
 Dw Offset Op154, 0

 Db 6,'FLDL2E',0,0,0, 0, 1
 Dw Offset Op155, 0

 Db 6,'FLDL2T',0,0,0, 0, 1
 Dw Offset Op156, 0

 Db 6,'FLDLG2',0,0,0, 0, 1
 Dw Offset Op157, 0

 Db 6,'FLDLN2',0,0,0, 0, 1
 Dw Offset Op158, 0

 Db 5,'FLDPI',0,0,0,0, 0, 1
 Dw Offset Op159, 0

 Db 4,'FLDZ',0,0,0,0,0, 0, 1
 Dw Offset Op160, 0

 Db 4,'FMUL',0,0,0,0,0, 2, 8
 Dw Offset Op161, 0

 Db 5,'FIMUL',0,0,0,0, 2, 4
 Dw Offset Op162, 0

 Db 5,'FMULP',0,0,0,0, 2, 2
 Dw Offset Op163, 0

 Db 4,'FNOP',0,0,0,0,0, 0, 1
 Dw Offset Op164, 0

 Db 6,'FNCLEX',0,0,0, 0, 1
 Dw Offset Op217, 0

 Db 6,'FNDISI',0,0,0, 0, 1
 Dw Offset Op218, 0

 Db 5,'FNENI',0,0,0,0, 0, 1
 Dw Offset Op219, 0

 Db 6,'FNINIT',0,0,0, 0, 1
 Dw Offset Op220, 0

 Db 6,'FNSAVE',0,0,0, 1, 1
 Dw Offset Op221, 0

 Db 6,'FNSTCW',0,0,0, 1, 1
 Dw Offset Op222, 0

 Db 7,'FNSTENV',0,0, 1, 1
 Dw Offset Op223, 0

 Db 6,'FNSTSW',0,0,0, 1, 1
 Dw Offset Op224, 0

 Db 6,'FPATAN',0,0,0, 0, 1
 Dw Offset Op165, 0

 Db 5,'FPREM',0,0,0,0, 0, 1
 Dw Offset Op166, 0

 Db 5,'FPTAN',0,0,0,0, 0, 1
 Dw Offset Op167, 0

 Db 7,'FRNDINT',0,0, 0, 1
 Dw Offset Op168, 0

 Db 6,'FRSTOR',0,0,0, 1, 1
 Dw Offset Op169, 0

 Db 5,'FSAVE',0,0,0,0, 1, 1
 Dw Offset Op171, 0

 Db 6,'FSCALE',0,0,0, 0, 1
 Dw Offset Op172, 0

 Db 5,'FSQRT',0,0,0,0, 0, 1
 Dw Offset Op173, 0

 Db 3,'FST',0,0,0,0,0,0, 2, 5
 Dw Offset Op174, 0

 Db 4,'FIST',0,0,0,0,0, 2, 2
 Dw Offset Op175, 0

 Db 5,'FSTCW',0,0,0,0, 1, 1
 Dw Offset Op176, 0

 Db 6,'FSTENV',0,0,0, 1, 1
 Dw Offset Op177, 0

 Db 4,'FSTP',0,0,0,0,0, 2, 6
 Dw Offset Op178, 0

 Db 5,'FISTP',0,0,0,0, 2, 3
 Dw Offset Op179, 0

 Db 5,'FBSTP',0,0,0,0, 2, 1
 Dw Offset Op180, 0

 Db 5,'FSTSW',0,0,0,0, 1, 1
 Dw Offset Op181, 0

 Db 4,'FTST',0,0,0,0,0, 0, 1
 Dw Offset Op182, 0

 Db 5,'FWAIT',0,0,0,0, 0, 1
 Dw Offset Op183, 0

 Db 4,'FXAM',0,0,0,0,0, 0, 1
 Dw Offset Op184, 0

 Db 4,'FXCH',0,0,0,0,0, 0, 4
 Dw Offset Op185, 0

 Db 7,'FXTRACT',0,0, 0, 1
 Dw Offset Op186, 0

 Db 5,'FYL2X',0,0,0,0, 0, 1
 Dw Offset Op170, 0

 Db 7,'FYL2XP1',0,0, 0, 1
 Dw Offset Op187, 0
