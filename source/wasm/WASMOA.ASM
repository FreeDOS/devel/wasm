;Wolfware Assembler
;Copyright (c) 1985-1991 Eric Tauck. All rights reserved.

;===============================================;
;      Wolfware Assembler Instruction Set       ;
; Instruction data. Contains the data for each  ;
; individual variation of each instruction.     ;
; Has all relevant data to locate and encode    ;
; an instruction.                               ;
;===============================================;

;----- table stucture

Entry_Len Equ 10        ;size of single entry
Dt_Off Equ 0            ;destination type offset
St_Off Equ 2            ;source type offset
Os_Off Equ 4            ;size offset
Ot_Off Equ 6            ;flag offset
Vl_Off Equ 8            ;op-code offset

;----- format: DTYPE STYPE OPSIZE OPTYPE2 OPVAL
;----- size: word word word word word

 Ds $ And 1             ;puts the table on an even address, declares
                        ;one byte if presently at odd location

;----- MOV

Op1 Dw 0004h, 0200h, 0003h, 0645h, 00a0h ;acum,mem (d,w)
 Dw 0212h, 0002h, 0003h, 0643h, 0088h ;reg/mem/addr,reg (d,w)
 Dw 0002h, 0020h, 0003h, 0441h, 00b0h ;reg,immed (w)
 Dw 0212h, 0020h, 0003h, 0442h, 00c6h ;reg/mem/addr,immed (w)
 Dw 0212h, 0008h, 0002h, 0243h, 008ch ;reg/mem/addr,seg (d)

;----- MOVSB

Op2 Dw 0000h, 0000h, 0000h, 0000h, 00a4h ;no operands

;----- MOVSW

Op3 Dw 0000h, 0000h, 0000h, 0000h, 00a5h ;no operands

;----- CALL

Op4 Dw 0080h, 0001h, 0002h, 0004h, 00e8h ;near,none
 Dw 0020h, 0020h, 0002h, 0040h, 009ah ;immed,immed
 Dw 0212h, 0001h, 0002h, 0052h, 00ffh ;reg/mem/addr,none
 Dw 0210h, 0001h, 0004h, 005ah, 00ffh ;mem/addr,none

;----- RET

Op5 Dw 0001h, 0080h, 0000h, 0000h, 00c3h ;none,near
 Dw 0020h, 0080h, 0202h, 0040h, 00c2h ;immed,near
 Dw 0001h, 0100h, 0000h, 0000h, 00cbh ;none,far
 Dw 0020h, 0100h, 0202h, 0040h, 00cah ;immed,far

;----- REP

Op6 Dw 0000h, 0000h, 0000h, 0000h, 00f3h ;no operands

;----- REPE

Op7 Dw 0000h, 0000h, 0000h, 0000h, 00f3h ;no operands

;----- REPZ

Op8 Dw 0000h, 0000h, 0000h, 0000h, 00f3h ;no operands

;----- REPNE

Op9 Dw 0000h, 0000h, 0000h, 0000h, 00f2h ;no operands

;----- REPNZ

Op10 Dw 0000h, 0000h, 0000h, 0000h, 00f2h ;no operands

;----- RETF

Op35 Dw 0001h, 0000h, 0000h, 0000h, 00cbh ;none
 Dw 0020h, 0000h, 0002h, 0040h, 00cah ;immed

;----- RETN

Op37 Dw 0001h, 0000h, 0000h, 0000h, 00c3h ;none
 Dw 0020h, 0000h, 0002h, 0040h, 00c2h ;immed


;----- PROC

Op11 Dw 0180h, 0000h, 0000h, 0080h, 0000h ;near/far

;----- ENDP

Op12 Dw 0000h, 0000h, 0000h, 0080h, 0001h ;no operands

;----- DB

Op13 Dw 0000h, 0000h, 0000h, 0080h, 0003h ;no operands

;----- DW

Op14 Dw 0000h, 0000h, 0000h, 0080h, 0004h ;no operands

;----- CMP

Op15 Dw 0212h, 0002h, 0003h, 0643h, 0038h ;reg/mem/addr,reg (d,w)
 Dw 0004h, 0020h, 0003h, 0440h, 003ch ;acum,immed (w)
 Dw 0212h, 0020h, 0003h, 0c7ah, 0080h ;reg/mem/addr,immed (s,w)

;----- CMPSB

Op16 Dw 0000h, 0000h, 0000h, 0000h, 00a6h ;no operands

;----- CMPSW

Op17 Dw 0000h, 0000h, 0000h, 0000h, 00a7h ;no operands

;----- JE/JZ

Op18 Dw 0080h, 0000h, 0001h, 0004h, 0074h ;near

;----- JMPS

Op19 Dw 0080h, 0000h, 0001h, 0004h, 00ebh ;near

;----- JMP

Op20 Dw 0080h, 0001h, 0002h, 0004h, 00e9h ;near,none
 Dw 0020h, 0020h, 0002h, 0040h, 00eah ;immed,immed
 Dw 0212h, 0001h, 0002h, 0062h, 00ffh ;reg/mem/addr,none
 Dw 0210h, 0001h, 0004h, 006ah, 00ffh ;mem/addr,none

;----- EQU

Op21 Dw 0020h, 0000h, 0000h, 0080h, 0006h ;immed

;----- POP

Op22 Dw 0002h, 0000h, 0002h, 0041h, 0058h ;reg
 Dw 0008h, 0000h, 0002h, 0041h, 0007h ;seg
 Dw 0212h, 0000h, 0002h, 0042h, 008fh ;reg/mem/addr

;----- POPA

Op229 Dw 0000h, 0000h, 0000h, 0000h, 0061h ;no operands

;----- POPF

Op23 Dw 0000h, 0000h, 0000h, 0000h, 009dh ;no operands

;----- PUSH

Op24 Dw 0002h, 0000h, 0002h, 0041h, 0050h ;reg
 Dw 0008h, 0000h, 0002h, 0041h, 0006h ;seg
 Dw 0212h, 0000h, 0002h, 0072h, 00ffh ;reg/mem/addr
 Dw 0020h, 0000h, 0001h, 0040h, 006Ah ;immed
 Dw 0020h, 0000h, 0002h, 0040h, 0068h ;immed

;----- PUSHA

Op228 Dw 0000h, 0000h, 0000h, 0000h, 0060h ;no operands

;----- PUSHF

Op25 Dw 0000h, 0000h, 0000h, 0000h, 009ch ;no operands

;----- ADD

Op26 Dw 0212h, 0002h, 0003h, 0643h, 0000h ;reg/mem/addr,reg (d,w)
 Dw 0004h, 0020h, 0003h, 0440h, 0004h ;acum,immed (w)
 Dw 0212h, 0020h, 0003h, 0c42h, 0080h ;reg/mem/addr,immed (s,w)

;----- ADC

Op27 Dw 0212h, 0002h, 0003h, 0643h, 0010h ;reg/mem/addr,reg (d,w)
 Dw 0004h, 0020h, 0003h, 0440h, 0014h ;acum,immed (w)
 Dw 0212h, 0020h, 0003h, 0c52h, 0080h ;reg/mem/addr,immed (s,w)

;----- SUB

Op28 Dw 0212h, 0002h, 0003h, 0643h, 0028h ;reg/mem/addr,reg (d,w)
 Dw 0004h, 0020h, 0003h, 0440h, 002ch ;acum,immed (w)
 Dw 0212h, 0020h, 0003h, 0c6ah, 0080h ;reg/mem/addr,immed (s,w)

;----- INC

Op29 Dw 0002h, 0000h, 0002h, 0041h, 0040h ;reg
 Dw 0212h, 0000h, 0003h, 0442h, 00feh ;mem/reg/addr (w)

;----- IN

Op30 Dw 0004h, 0020h, 0603h, 2440h, 00e4h ;acum,immed
 Dw 0004h, 0002h, 0603h, 0440h, 00ech ;acum,reg (DX)

;----- INCLUDE

Op31 Dw 0040h, 0000h, 0000h, 0080h, 000ah ;string

;----- INSB

Op230 Dw 0000h, 0000h, 0000h, 0000h, 006ch ;no operands

;----- INSW

Op238 Dw 0000h, 0000h, 0000h, 0000h, 006dh ;no operands

;----- INT

Op32 Dw 0020h, 0000h, 0001h, 0040h, 00cdh ;immed

;----- INT3

Op120 Dw 0000h, 0000h, 0000h, 0000h, 00cch ;no operands

;----- INTO

Op33 Dw 0000h, 0000h, 0000h, 0000h, 00ceh ;no operands

;----- DEC

Op34 Dw 0002h, 0000h, 0002h, 0041h, 0048h ;reg
 Dw 0212h, 0000h, 0003h, 044ah, 00feh ;mem/reg/addr (w)

;----- JNE/JNZ

Op36 Dw 0080h, 0000h, 0001h, 0004h, 0075h ;near

;----- JNB/JAE/JNC

Op38 Dw 0080h, 0000h, 0001h, 0004h, 0073h ;near

;----- JNO

Op39 Dw 0080h, 0000h, 0001h, 0004h, 0071h ;near

;----- JNP/JPO

Op40 Dw 0080h, 0000h, 0001h, 0004h, 007bh ;near

;----- JNS

Op41 Dw 0080h, 0000h, 0001h, 0004h, 0079h ;near

;----- OR

Op42 Dw 0212h, 0002h, 0003h, 0643h, 0008h ;reg/mem/addr,reg (d,w)
 Dw 0004h, 0020h, 0003h, 0440h, 000ch ;acum,immed (w)
 Dw 0212h, 0020h, 0003h, 044ah, 0080h ;reg/mem/addr,immed (w)

;----- ORG

Op43 Dw 0020h, 0000h, 0003h, 00c0h, 0002h ;immed

;----- STOSB

Op44 Dw 0000h, 0000h, 0000h, 0000h, 00aah ;no operands

;----- STOSW

Op45 Dw 0000h, 0000h, 0000h, 0000h, 00abh ;no operands

;----- STC

Op46 Dw 0000h, 0000h, 0000h, 0000h, 00f9h ;no operands

;----- STD

Op47 Dw 0000h, 0000h, 0000h, 0000h, 00fdh ;no operands

;----- STI

Op48 Dw 0000h, 0000h, 0000h, 0000h, 00fbh ;no operands

;----- STR

Op241 Dw 0212h, 0001h, 0002h, 004ah, 0f00h ;mem/reg/addr,none

;----- LODSB

Op49 Dw 0000h, 0000h, 0000h, 0000h, 00ach ;no operands

;----- LODSW

Op50 Dw 0000h, 0000h, 0000h, 0000h, 00adh ;no operands

;----- LOCK

Op51 Dw 0000h, 0000h, 0000h, 0000h, 00f0h ;no operands

;----- LOOP

Op52 Dw 0080h, 0000h, 0001h, 0004h, 00e2h ;near

;----- LOOPE/LOOPZ

Op53 Dw 0080h, 0000h, 0001h, 0004h, 00e1h ;near

;----- LOOPNE/LOOPNZ

Op55 Dw 0080h, 0000h, 0001h, 0004h, 00e0h ;near

;----- AAA

Op57 Dw 0000h, 0000h, 0000h, 0000h, 0037h ;no operands

;----- AAD

Op61 Dw 0000h, 0000h, 0000h, 0000h, 0d50ah ;no operands

;----- AAM

Op59 Dw 0000h, 0000h, 0000h, 0000h, 0d40ah ;no operands

;----- AAS

Op60 Dw 0000h, 0000h, 0000h, 0000h, 003fh ;no operands

;----- AND

Op58 Dw 0212h, 0002h, 0003h, 0643h, 0020h ;reg/mem/addr,reg (d,w)
 Dw 0004h, 0020h, 0003h, 0440h, 0024h ;acum,immed (w)
 Dw 0212h, 0020h, 0003h, 0462h, 0080h ;reg/mem/addr,immed (w)

;----- ARPL

Op247 Dw 0212h, 0002h, 0002h, 0043h, 0063h ;mem/reg/addr,reg

;----- BOUND

Op225 Dw 0002h, 0212h, 0202h, 1043h, 0062h ;reg,reg/mem/addr

;----- CBW

Op62 Dw 0000h, 0000h, 0000h, 0000h, 0098h ;no operands

;----- CLC

Op63 Dw 0000h, 0000h, 0000h, 0000h, 00f8h ;no operands

;----- CLD

Op64 Dw 0000h, 0000h, 0000h, 0000h, 00fch ;no operands

;----- CLI

Op65 Dw 0000h, 0000h, 0000h, 0000h, 00fah ;no operands

;----- CLTS

Op232 Dw 0000h, 0000h, 0000h, 0000h, 0f06h ;no operands

;----- CMC

Op66 Dw 0000h, 0000h, 0000h, 0000h, 00f5h ;no operands

;----- CS:

Op213 Dw 0000h, 0000h, 0000h, 0000h, 002Eh ;no operands

;----- CWD

Op67 Dw 0000h, 0000h, 0000h, 0000h, 0099h ;no operands

;----- DAA

Op68 Dw 0000h, 0000h, 0000h, 0000h, 0027h ;no operands

;----- DAS

Op69 Dw 0000h, 0000h, 0000h, 0000h, 002fh ;no operands

;----- DIV

Op70 Dw 0212h, 0001h, 0003h, 0472h, 00f6h ;mem/reg/addr,none (w)
 Dw 0004h, 0212h, 0003h, 1472h, 00f6h ;acum,mem/reg/addr (w)

;----- DS

Op71 Dw 0020h, 0021h, 0203h, 00c0h, 0005h ;immed,immed/none

;----- DS:

Op214 Dw 0000h, 0000h, 0000h, 0000h, 003Eh ;no operands

;----- ELSE

Op197 Dw 0000h, 0000h, 0000h, 4080h, 0018h ;no operands

;----- ELSEIF

Op196 Dw 0020h, 0000h, 0000h, 4080h, 0017h ;immed

;----- ENDIF

Op198 Dw 0000h, 0000h, 0000h, 4080h, 0019h ;no operands

;----- ENDM

Op193 Dw 0000h, 0000h, 0000h, 4080h, 0014h ;no operands

;----- ENTER

Op226 Dw 0020h, 0020h, 0203h, 00c0h, 0028h ;immed,immed

;----- ERROR

Op210 Dw 0040h, 0000h, 0000h, 4080h, 0025h ;string

;----- ERRORMAX

Op192 Dw 0020h, 0000h, 0003h, 40c0h, 0012h ;string

;----- ES:

Op215 Dw 0000h, 0000h, 0000h, 0000h, 0026h ;no operands

;----- EXPAND+

Op200 Dw 0000h, 0000h, 0000h, 4080h, 001bh ;no operands

;----- EXPAND-

Op201 Dw 0000h, 0000h, 0000h, 4080h, 001ch ;no operands

;----- FLAGALL+

Op208 Dw 0000h, 0000h, 0000h, 4080h, 0023h ;no operands

;----- FLAGALL-

Op209 Dw 0000h, 0000h, 0000h, 4080h, 0024h ;no operands

;----- HLT

Op72 Dw 0000h, 0000h, 0000h, 0000h, 00f4h ;no operands

;----- IDIV

Op73 Dw 0212h, 0001h, 0003h, 047ah, 00f6h ;mem/reg/addr,none (w)
 Dw 0004h, 0212h, 0003h, 147ah, 00f6h ;acum,mem/reg/addr (w)

;----- IF

Op195 Dw 0020h, 0000h, 0000h, 4080h, 0016h ;immed

;----- IFN

Op212 Dw 0020h, 0000h, 0000h, 4080h, 0027h ;immed

;----- IMUL

Op74 Dw 0212h, 0001h, 0003h, 046ah, 00f6h ;mem/reg/addr,none (w)
 Dw 0004h, 0212h, 0003h, 146ah, 00f6h ;acum, mem/reg/addr (w)
 Dw 0002h, 0212h, 0002h, 10c3h, 0029h ;reg,mem/reg/addr<,immed>
 Dw 0002h, 0020h, 0002h, 00c3h, 0029h ;reg,immed

;----- IRET

Op75 Dw 0000h, 0000h, 0000h, 0000h, 00cfh ;no operands

;----- JNBE/JA

Op76 Dw 0080h, 0000h, 0001h, 0004h, 0077h ;near

;----- JB/JNAE/JC

Op78 Dw 0080h, 0000h, 0001h, 0004h, 0072h ;near

;----- JBE/JNA

Op79 Dw 0080h, 0000h, 0001h, 0004h, 0076h ;near

;----- JCXZ

Op80 Dw 0080h, 0000h, 0001h, 0004h, 00e3h ;near

;----- JG/JNLE

Op54 Dw 0080h, 0000h, 0001h, 0004h, 007fh ;near

;----- JUMP+

Op202 Dw 0000h, 0000h, 0000h, 4080h, 001dh ;no operands

;----- JUMP-

Op203 Dw 0000h, 0000h, 0000h, 4080h, 001eh ;no operands

;----- JNL/JGE

Op81 Dw 0080h, 0000h, 0001h, 0004h, 007dh ;near

;----- JL/JNGE

Op82 Dw 0080h, 0000h, 0001h, 0004h, 007ch ;near

;----- JLE/JNG

Op83 Dw 0080h, 0000h, 0001h, 0004h, 007eh ;near

;----- JO

Op84 Dw 0080h, 0000h, 0001h, 0004h, 0070h ;near

;----- JP/JPE

Op85 Dw 0080h, 0000h, 0001h, 0004h, 007ah ;near

;----- JS

Op87 Dw 0080h, 0000h, 0001h, 0004h, 0078h ;near

;----- LABEL

Op86 Dw 0001h, 0000h, 00ffh, 00c0h, 000bh ;none
 Dw 0180h, 0000h, 0000h, 0080h, 000bh ;near/far

;----- LAHF

Op89 Dw 0000h, 0000h, 0000h, 0000h, 009fh ;no operands

;----- LAR

Op248 Dw 0002h, 0212h, 0002h, 1043h, 0f02h ;reg,mem/reg/addr

;----- LDS

Op90 Dw 0002h, 0212h, 0202h, 1043h, 00c5h ;reg,reg/mem/addr

;----- LEA

Op91 Dw 0002h, 0212h, 0202h, 1043h, 008dh ;reg,reg/mem/addr

;----- LEAVE

Op227 Dw 0000h, 0000h, 0000h, 0000h, 00c9h ;no operands

;----- LES

Op92 Dw 0002h, 0212h, 0202h, 1043h, 00c4h ;reg,reg/mem/addr

;----- LGDT

Op233 Dw 0210h, 0001h, 0008h, 0052h, 0f01h ;mem/addr,none

;----- LIDT

Op234 Dw 0210h, 0001h, 0008h, 005ah, 0f01h ;mem/addr,none

;----- LINESIZE

Op114 Dw 0021h, 0000h, 0000h, 4080h, 0007h ;immed/none

;----- LIST+

Op93 Dw 0000h, 0000h, 0000h, 4080h, 0008h ;no operands

;----- LIST-

Op94 Dw 0000h, 0000h, 0000h, 4080h, 0009h ;no operands

;----- LLDT

Op235 Dw 0212h, 0001h, 0002h, 0052h, 0f00h ;mem/reg/addr,none

;----- LMSW

Op236 Dw 0212h, 0001h, 0002h, 0072h, 0f01h ;mem/reg/addr,none

;----- LSL

Op249 Dw 0002h, 0212h, 0002h, 1043h, 0f03h ;reg,mem/reg/addr

;----- LTR

Op237 Dw 0212h, 0001h, 0002h, 005Ah, 0f00h ;mem/reg/addr,none

;----- MACRO

Op194 Dw 0000h, 0000h, 0000h, 4080h, 0013h ;no operands

;----- MUL

Op95 Dw 0212h, 0001h, 0003h, 0462h, 00f6h ;mem/reg/addr,none (w)
 Dw 0004h, 0212h, 0003h, 1462h, 00f6h ;acum,mem/reg/addr (w)

;----- NEG

Op96 Dw 0212h, 0000h, 0003h, 045ah, 00f6h ;mem/reg/addr (w)

;----- NEXTIF

Op199 Dw 0020h, 0000h, 0000h, 4080h, 001ah ;immed

;----- NOP

Op97 Dw 0000h, 0000h, 0000h, 0000h, 0090h ;no operands

;----- NOT

Op98 Dw 0212h, 0000h, 0003h, 0452h, 00f6h ;mem/reg/addr (w)

;----- OUT

Op99 Dw 0020h, 0004h, 0903h, 3440h, 00e6h ;immed,acum
 Dw 0002h, 0004h, 0903h, 1440h, 00eeh ;reg (DX),acum

;----- OUTSB

Op231 Dw 0000h, 0000h, 0000h, 0000h, 006eh ;no operands

;----- OUTSW

Op239 Dw 0000h, 0000h, 0000h, 0000h, 006fh ;no operands

;----- PAGE

Op56 Dw 0001h, 0001h, 0000h, 4080h, 000ch ;none,none
 Dw 0020h, 0041h, 0203h, 40c0h, 000ch ;immed,string/none
 Dw 0040h, 0001h, 0000h, 4080h, 000ch ;string,none

;----- PAGE+

Op118 Dw 0001h, 0001h, 0000h, 4080h, 000eh ;none,none
 Dw 0020h, 0041h, 0203h, 40c0h, 000eh ;immed,string/none
 Dw 0040h, 0001h, 0000h, 4080h, 000eh ;string,none

;----- PAGE-

Op119 Dw 0000h, 0000h, 0000h, 4080h, 000fh ;no operands

;----- PAGESIZE

Op88 Dw 0020h, 0021h, 0000h, 4080h, 000dh ;immed,immed/none
     Dw 0001h, 0020h, 0000h, 4080h, 000dh ;none,immed

;----- RCL

Op100 Dw 0212h, 0001h, 0003h, 0452h, 00d0h ;mem/reg/addr,none (w)
 Dw 0212h, 0002h, 0603h, 0452h, 00d2h ;reg/mem/addr,reg (w)
 Dw 0212h, 0020h, 0603h, 2452h, 00c0h ;reg/mem/addr,immed (w)

;----- RCR

Op101 Dw 0212h, 0001h, 0003h, 045ah, 00d0h ;mem/reg/addr,none (w)
 Dw 0212h, 0002h, 0603h, 045ah, 00d2h ;mem/reg/addr,reg (w)
 Dw 0212h, 0020h, 0603h, 245ah, 00c0h ;mem/reg/addr,immed (w)

;----- RESETC

Op211 Dw 0021h, 0021h, 0000h, 4080h, 0026h ;none/immed, none/immed

;----- ROL

Op102 Dw 0212h, 0001h, 0003h, 0442h, 00d0h ;mem/reg/addr,none (w)
 Dw 0212h, 0002h, 0603h, 0442h, 00d2h ;mem/reg/addr,reg (w)
 Dw 0212h, 0020h, 0603h, 2442h, 00c0h ;mem/reg/addr,immed (w)

;----- ROR

Op103 Dw 0212h, 0001h, 0003h, 044ah, 00d0h ;mem/reg/addr,none (w)
 Dw 0212h, 0002h, 0603h, 044ah, 00d2h ;mem/reg/addr,reg (w)
 Dw 0212h, 0020h, 0603h, 244ah, 00c0h ;mem/reg/addr,immed (w)

;----- SAHF

Op104 Dw 0000h, 0000h, 0000h, 0000h, 009eh ;no operands

;----- SAR

Op105 Dw 0212h, 0001h, 0003h, 047ah, 00d0h ;mem/reg/addr,none (w)
 Dw 0212h, 0002h, 0603h, 047ah, 00d2h ;mem/reg/addr,reg (w)
 Dw 0212h, 0020h, 0603h, 247ah, 00c0h ;mem/reg/addr,immed (w)

;----- SBB

Op106 Dw 0212h, 0002h, 0003h, 0643h, 0018h ;reg/mem/addr,reg (d,w)
 Dw 0004h, 0020h, 0003h, 0440h, 001ch ;acum,immed (w)
 Dw 0212h, 0020h, 0003h, 0c5ah, 0080h ;reg/mem/addr,immed (s,w)

;----- SCASB

Op107 Dw 0000h, 0000h, 0000h, 0000h, 00aeh ;no operands

;----- SCASW

Op108 Dw 0000h, 0000h, 0000h, 0000h, 00afh ;no operands

;----- SEG

Op109 Dw 0008h, 0000h, 0000h, 0001h, 0026h ;seg

;----- SGDT

Op240 Dw 0210h, 0001h, 0008h, 0042h, 0f01h ;mem/addr,none

;----- SHL/SAL

Op110 Dw 0212h, 0001h, 0003h, 0462h, 00d0h ;mem/reg/addr,none (w)
 Dw 0212h, 0002h, 0603h, 0462h, 00d2h ;mem/reg/addr,reg (w)
 Dw 0212h, 0020h, 0603h, 2462h, 00c0h ;mem/reg/addr,reg (w)

;----- SHR

Op111 Dw 0212h, 0001h, 0003h, 046ah, 00d0h ;mem/reg/addr,none (w)
 Dw 0212h, 0002h, 0603h, 046ah, 00d2h ;mem/reg/addr,reg (w)
 Dw 0212h, 0020h, 0603h, 246ah, 00c0h ;mem/reg/addr,immed (w)

;----- SIDT

Op244 Dw 0210h, 0001h, 0008h, 004ah, 0f01h ;mem/addr,none

;----- SLDT

Op245 Dw 0212h, 0001h, 0002h, 0042h, 0f00h ;mem/reg/addr,none

;----- SMSW

Op246 Dw 0212h, 0001h, 0002h, 0062h, 0f01h ;mem/reg/addr,none

;----- SS:

Op216 Dw 0000h, 0000h, 0000h, 0000h, 0036h ;no operands

;----- SUBTITLE

Op191 Dw 0040h, 0000h, 0000h, 4080h, 0011h ;string

;----- SYMDUMP+

Op206 Dw 0000h, 0000h, 0000h, 4080h, 0021h ;no operands

;----- SYMDUMP-

Op207 Dw 0000h, 0000h, 0000h, 4080h, 0022h ;no operands

;----- TEST

Op112 Dw 0212h, 0002h, 0003h, 0443h, 0084h ;mem/reg/addr,reg (w)
 Dw 0002h, 0212h, 0003h, 1443h, 0084h ;reg,mem/reg/addr (w)
 Dw 0004h, 0020h, 0003h, 0440h, 00a8h ;acum,immed (w)
 Dw 0212h, 0020h, 0003h, 0442h, 00f6h ;reg/mem/addr,immed (w)

;----- TITLE

Op190 Dw 0000h, 0000h, 0000h, 4080h, 0010h ;no operands

;----- UNUSED+

Op204 Dw 0000h, 0000h, 0000h, 4080h, 001fh ;no operands

;----- UNUSED-

Op205 Dw 0000h, 0000h, 0000h, 4080h, 0020h ;no operands

;----- VERR

Op242 Dw 0212h, 0001h, 0002h, 0062h, 0f00h ;mem/reg/addr,none

;----- VERW

Op243 Dw 0212h, 0001h, 0002h, 006ah, 0f00h ;mem/reg/addr,none

;----- WAIT

Op113 Dw 0000h, 0000h, 0000h, 0000h, 009bh ;no operands

;----- XCHG

Op115 Dw 0002h, 0004h, 0002h, 0041h, 0090h ;reg,acum
 Dw 0004h, 0002h, 0002h, 1041h, 0090h ;acum,reg
 Dw 0212h, 0002h, 0003h, 0443h, 0086h ;reg/mem/addr,reg
 Dw 0002h, 0212h, 0003h, 1443h, 0086h ;reg,reg/mem/addr

;----- XLAT

Op116 Dw 0000h, 0000h, 0000h, 0000h, 00d7h ;no operands

;----- XOR

Op117 Dw 0212h, 0002h, 0003h, 0643h, 0030h ;reg/mem/addr,reg (d,w)
 Dw 0004h, 0020h, 0003h, 0440h, 0034h ;acum,immed (w)
 Dw 0212h, 0020h, 0003h, 0472h, 0080h ;reg/mem/addr,immed (w)

;----- F2XM1

Op124 Dw 0000h, 0000h, 0000h, 0100h, 0d9f0h ;no operands

;----- FABS

Op125 Dw 0000h, 0000h, 0000h, 0100h, 0d9e1h ;no operands

;----- FADD

Op126 Dw 0001h, 0001h, 0000h, 0100h, 0dec1h ;none,none
 Dw 0400h, 0001h, 0000h, 0102h, 00d8h ;ST(i),none
 Dw 0800h, 0400h, 0000h, 1102h, 00d8h ;ST(0),ST(i)
 Dw 0400h, 0800h, 0000h, 0102h, 00dch ;ST(i),ST(0)
 Dw 0210h, 0001h, 0004h, 0142h, 00d8h ;mem/addr,none
 Dw 0800h, 0210h, 0804h, 1142h, 00d8h ;ST(0),mem/addr
 Dw 0210h, 0001h, 0008h, 0142h, 00dch ;mem/addr,none
 Dw 0800h, 0210h, 0808h, 1142h, 00dch ;ST(0),mem/addr

;----- FIADD

Op127 Dw 0210h, 0001h, 0002h, 0142h, 00deh ;mem/addr,none
 Dw 0800h, 0210h, 0802h, 1142h, 00deh ;ST(0),mem/addr
 Dw 0210h, 0001h, 0004h, 0142h, 00dah ;mem/addr,none
 Dw 0800h, 0210h, 0804h, 1142h, 00dah ;ST(0),mem/addr

;----- FADDP

Op148 Dw 0001h, 0001h, 0000h, 0100h, 0dec1h ;none,none
 Dw 0400h, 0801h, 0000h, 0102h, 00deh ;ST(i),ST(0)

;----- FCHS

Op128 Dw 0000h, 0000h, 0000h, 0100h, 0d9e0h ;no operands

;----- FCLEX

Op129 Dw 0000h, 0000h, 0000h, 0100h, 0dbe2h ;no operands

;----- FCOM

Op130 Dw 0001h, 0001h, 0000h, 0100h, 0d8d1h ;none,none
 Dw 0400h, 0001h, 0000h, 0112h, 00d8h ;ST(i),none
 Dw 0800h, 0400h, 0000h, 1112h, 00d8h ;ST(0),ST(i)
 Dw 0210h, 0001h, 0004h, 0152h, 00d8h ;mem/addr,none
 Dw 0800h, 0210h, 0804h, 1152h, 00d8h ;ST(0),mem/addr
 Dw 0210h, 0001h, 0008h, 0152h, 00dch ;mem/addr,none
 Dw 0800h, 0210h, 0808h, 1152h, 00dch ;ST(0),mem/addr

;----- FICOM

Op131 Dw 0210h, 0001h, 0002h, 0152h, 00deh ;mem/addr,none
 Dw 0800h, 0210h, 0802h, 1152h, 00deh ;ST(0),mem/addr
 Dw 0210h, 0001h, 0004h, 0152h, 00dah ;mem/addr,none
 Dw 0800h, 0210h, 0804h, 1152h, 00dah ;ST(0),mem/addr

;----- FCOMP

Op132 Dw 0001h, 0001h, 0000h, 0100h, 0d8d9h ;none,none
 Dw 0400h, 0001h, 0000h, 011ah, 00d8h ;ST(i),none
 Dw 0800h, 0400h, 0000h, 111ah, 00d8h ;ST(0),ST(i)
 Dw 0210h, 0001h, 0004h, 015ah, 00d8h ;mem/addr,none
 Dw 0800h, 0210h, 0804h, 115ah, 00d8h ;ST(0),mem/addr
 Dw 0210h, 0001h, 0008h, 015ah, 00dch ;mem/addr,none
 Dw 0800h, 0210h, 0808h, 115ah, 00dch ;ST(0),mem/addr

;----- FICOMP

Op133 Dw 0210h, 0001h, 0002h, 015ah, 00deh ;mem/addr,none
 Dw 0800h, 0210h, 0802h, 115ah, 00deh ;ST(0),mem/addr
 Dw 0210h, 0001h, 0004h, 015ah, 00dah ;mem/addr,none
 Dw 0800h, 0210h, 0804h, 115ah, 00dah ;ST(0),mem/addr

;----- FCOMPP

Op134 Dw 0000h, 0000h, 0000h, 0100h, 0ded9h ;no operands

;----- FDECSTP

Op135 Dw 0000h, 0000h, 0000h, 0100h, 0d9f6h ;no operands

;----- FDISI

Op136 Dw 0000h, 0000h, 0000h, 0100h, 0dbe1h ;no operands

;----- FDIV

Op137 Dw 0001h, 0001h, 0000h, 0100h, 0def9h ;none,none
 Dw 0400h, 0001h, 0000h, 0132h, 00d8h ;ST(i),none
 Dw 0800h, 0400h, 0000h, 1132h, 00d8h ;ST(0),ST(i)
 Dw 0400h, 0800h, 0000h, 013ah, 00dch ;ST(i),ST(0)
 Dw 0210h, 0001h, 0004h, 0172h, 00d8h ;mem/addr,none
 Dw 0800h, 0210h, 0804h, 1172h, 00d8h ;ST(0),mem/addr
 Dw 0210h, 0001h, 0008h, 0172h, 00dch ;mem/addr,none
 Dw 0800h, 0210h, 0808h, 1172h, 00dch ;ST(0),mem/addr

;----- FIDIV

Op138 Dw 0210h, 0001h, 0002h, 0172h, 00deh ;mem/addr,none
 Dw 0800h, 0210h, 0802h, 1172h, 00deh ;ST(0),mem/addr
 Dw 0210h, 0001h, 0004h, 0172h, 00dah ;mem/addr,none
 Dw 0800h, 0210h, 0804h, 1172h, 00dah ;ST(0),mem/addr

;----- FDIVP

Op139 Dw 0001h, 0001h, 0000h, 0100h, 0def9h ;none,none
 Dw 0400h, 0801h, 0000h, 013ah, 00deh ;ST(i),ST(0)

;----- FDIVR

Op140 Dw 0001h, 0001h, 0000h, 0100h, 0def1h ;none,none
 Dw 0400h, 0001h, 0000h, 013ah, 00d8h ;ST(i),none
 Dw 0800h, 0400h, 0000h, 113ah, 00d8h ;ST(0),ST(i)
 Dw 0400h, 0800h, 0000h, 0132h, 00dch ;ST(i),ST(0)
 Dw 0210h, 0001h, 0004h, 017ah, 00d8h ;mem/addr,none
 Dw 0800h, 0210h, 0804h, 117ah, 00d8h ;ST(0),mem/addr
 Dw 0210h, 0001h, 0008h, 017ah, 00dch ;mem/addr,none
 Dw 0800h, 0210h, 0808h, 117ah, 00dch ;ST(0),mem/addr

;----- FIDIVR

Op141 Dw 0210h, 0001h, 0002h, 017ah, 00deh ;mem/addr,none
 Dw 0800h, 0210h, 0802h, 117ah, 00deh ;ST(0),mem/addr
 Dw 0210h, 0001h, 0004h, 017ah, 00dah ;mem/addr,none
 Dw 0800h, 0210h, 0804h, 117ah, 00dah ;ST(0),mem/addr

;----- FDIVRP

Op189 Dw 0001h, 0001h, 0000h, 0100h, 0def1h ;none,none
 Dw 0400h, 0801h, 0000h, 0132h, 00deh ;ST(i),ST(0)

;----- FSUB

Op142 Dw 0001h, 0001h, 0000h, 0100h, 0dee9h ;none,none
 Dw 0400h, 0001h, 0000h, 0122h, 00d8h ;ST(i),none
 Dw 0800h, 0400h, 0000h, 1122h, 00d8h ;ST(0),ST(i)
 Dw 0400h, 0800h, 0000h, 012ah, 00dch ;ST(i),ST(0)
 Dw 0210h, 0001h, 0004h, 0162h, 00d8h ;mem/addr,none
 Dw 0800h, 0210h, 0804h, 1162h, 00d8h ;ST(0),mem/addr
 Dw 0210h, 0001h, 0008h, 0162h, 00dch ;mem/addr,none
 Dw 0800h, 0210h, 0808h, 1162h, 00dch ;ST(0),mem/addr

;----- FISUB

Op143 Dw 0210h, 0001h, 0002h, 0162h, 00deh ;mem/addr,none
 Dw 0800h, 0210h, 0802h, 1162h, 00deh ;ST(0),mem/addr
 Dw 0210h, 0001h, 0004h, 0162h, 00dah ;mem/addr,none
 Dw 0800h, 0210h, 0804h, 1162h, 00dah ;ST(0),mem/addr

;----- FSUBP

Op144 Dw 0001h, 0001h, 0000h, 0100h, 0dee9h ;none,none
 Dw 0400h, 0801h, 0000h, 012ah, 00deh ;ST(i),ST(0)

;----- FSUBR

Op145 Dw 0001h, 0001h, 0000h, 0100h, 0dee1h ;none,none
 Dw 0400h, 0001h, 0000h, 012ah, 00d8h ;ST(i),none
 Dw 0800h, 0400h, 0000h, 112ah, 00d8h ;ST(0),ST(i)
 Dw 0400h, 0800h, 0000h, 0122h, 00dch ;ST(i),ST(0)
 Dw 0210h, 0001h, 0004h, 016ah, 00d8h ;mem/addr,none
 Dw 0800h, 0210h, 0804h, 116ah, 00d8h ;ST(0),mem/addr
 Dw 0210h, 0001h, 0008h, 016ah, 00dch ;mem/addr,none
 Dw 0800h, 0210h, 0808h, 116ah, 00dch ;ST(0),mem/addr

;----- FISUBR

Op146 Dw 0210h, 0001h, 0002h, 016ah, 00deh ;mem/addr,none
 Dw 0800h, 0210h, 0802h, 116ah, 00deh ;ST(0),mem/addr
 Dw 0210h, 0001h, 0004h, 016ah, 00dah ;mem/addr,none
 Dw 0800h, 0210h, 0804h, 116ah, 00dah ;ST(0),mem/addr

;----- FSUBRP

Op188 Dw 0001h, 0001h, 0000h, 0100h, 0dee1h ;none,none
 Dw 0400h, 0801h, 0000h, 0122h, 00deh ;ST(i),ST(0)

;----- FENI

Op147 Dw 0000h, 0000h, 0000h, 0100h, 0dbe0h ;no operands

;----- FFREE

Op149 Dw 0001h, 0000h, 0000h, 0100h, 0ddc1h ;none
 Dw 0400h, 0000h, 0000h, 0102h, 00ddh ;ST(i)

;----- FINCSTP

Op150 Dw 0000h, 0000h, 0000h, 0100h, 0d9f7h ;no operands

;----- FINIT

Op151 Dw 0000h, 0000h, 0000h, 0100h, 0dbe3h ;no operands

;----- FLD

Op121 Dw 0001h, 0001h, 0000h, 0100h, 0d9c1h ;none,none
 Dw 0400h, 0001h, 0000h, 0102h, 00d9h ;ST(i),none
 Dw 0800h, 0400h, 0000h, 1102h, 00d9h ;ST(0),ST(i)
 Dw 0210h, 0001h, 0004h, 0142h, 00d9h ;mem/addr,none
 Dw 0800h, 0210h, 0804h, 1142h, 00d9h ;ST(0),mem/addr
 Dw 0210h, 0001h, 0008h, 0142h, 00ddh ;mem/addr,none
 Dw 0800h, 0210h, 0808h, 1142h, 00ddh ;ST(0),mem/addr
 Dw 0210h, 0001h, 0010h, 016ah, 00dbh ;mem/addr,none
 Dw 0800h, 0210h, 0810h, 116ah, 00dbh ;ST(0),mem/addr

;----- FILD

Op122 Dw 0210h, 0001h, 0002h, 0142h, 00dfh ;mem/addr,none
 Dw 0800h, 0210h, 0802h, 1142h, 00dfh ;ST(0),mem/addr
 Dw 0210h, 0001h, 0004h, 0142h, 00dbh ;mem/addr,none
 Dw 0800h, 0210h, 0804h, 1142h, 00dbh ;ST(0),mem/addr
 Dw 0210h, 0001h, 0008h, 016ah, 00dfh ;mem/addr,none
 Dw 0800h, 0210h, 0808h, 116ah, 00dfh ;ST(0),mem/addr

;----- FBLD

Op123 Dw 0210h, 0001h, 0010h, 0162h, 00dfh ;mem/addr,none
 Dw 0800h, 0210h, 0810h, 1162h, 00dfh ;ST(0),mem/addr

;----- FLD1

Op152 Dw 0000h, 0000h, 0000h, 0100h, 0d9e8h ;no operands

;----- FLDCW

Op153 Dw 0210h, 0000h, 0002h, 016ah, 00d9h ;mem/addr

;----- FLDENV

Op154 Dw 0210h, 0000h, 0000h, 0122h, 00d9h ;mem/addr

;----- FLDL2E

Op155 Dw 0000h, 0000h, 0000h, 0100h, 0d9eah ;no operands

;----- FLDL2T

Op156 Dw 0000h, 0000h, 0000h, 0100h, 0d9e9h ;no operands

;----- FLDLG2

Op157 Dw 0000h, 0000h, 0000h, 0100h, 0d9ech ;no operands

;----- FLDLN2

Op158 Dw 0000h, 0000h, 0000h, 0100h, 0d9edh ;no operands

;----- FLDPI

Op159 Dw 0000h, 0000h, 0000h, 0100h, 0d9ebh ;no operands

;----- FLDZ

Op160 Dw 0000h, 0000h, 0000h, 0100h, 0d9eeh ;no operands

;----- FMUL

Op161 Dw 0001h, 0001h, 0000h, 0100h, 0dec9h ;none,none
 Dw 0400h, 0001h, 0000h, 010ah, 00d8h ;ST(i),none
 Dw 0800h, 0400h, 0000h, 110ah, 00d8h ;ST(0),ST(i)
 Dw 0400h, 0800h, 0000h, 010ah, 00dch ;ST(i),ST(0)
 Dw 0210h, 0001h, 0004h, 014ah, 00d8h ;mem/addr,none
 Dw 0800h, 0210h, 0804h, 114ah, 00d8h ;ST(0),mem/addr
 Dw 0210h, 0001h, 0008h, 014ah, 00dch ;mem/addr,none
 Dw 0800h, 0210h, 0808h, 114ah, 00dch ;ST(0),mem/addr

;----- FIMUL

Op162 Dw 0210h, 0001h, 0002h, 014ah, 00deh ;mem/addr,none
 Dw 0800h, 0210h, 0802h, 114ah, 00deh ;ST(0),mem/addr
 Dw 0210h, 0001h, 0004h, 014ah, 00dah ;mem/addr,none
 Dw 0800h, 0210h, 0804h, 114ah, 00dah ;ST(0),mem/addr

;----- FMULP

Op163 Dw 0001h, 0001h, 0000h, 0100h, 0dec9h ;none,none
 Dw 0400h, 0801h, 0000h, 010ah, 00deh ;ST(i),ST(0)

;----- FNOP

Op164 Dw 0000h, 0000h, 0000h, 0100h, 0d9d0h ;no operands

;----- FNCLEX

Op217 Dw 0000h, 0000h, 0000h, 0000h, 0dbe2h ;no operands

;----- FNDISI

Op218 Dw 0000h, 0000h, 0000h, 0000h, 0dbe1h ;no operands

;----- FNENI

Op219 Dw 0000h, 0000h, 0000h, 0000h, 0dbe0h ;no operands

;----- FNINIT

Op220 Dw 0000h, 0000h, 0000h, 0000h, 0dbe3h ;no operands

;----- FNSAVE

Op221 Dw 0210h, 0000h, 0000h, 0032h, 00ddh ;mem/addr

;----- FNSTCW

Op222 Dw 0210h, 0000h, 0002h, 007ah, 00d9h ;mem/addr

;----- FNSTENV

Op223 Dw 0210h, 0000h, 0000h, 0032h, 00d9h ;mem/addr

;----- FNSTSW

Op224 Dw 0210h, 0000h, 0002h, 007ah, 00ddh ;mem/addr

;----- FPATAN

Op165 Dw 0000h, 0000h, 0000h, 0100h, 0d9f3h ;no operands

;----- FPREM

Op166 Dw 0000h, 0000h, 0000h, 0100h, 0d9f8h ;no operands

;----- FPTAN

Op167 Dw 0000h, 0000h, 0000h, 0100h, 0d9f2h ;no operands

;----- FRNDINT

Op168 Dw 0000h, 0000h, 0000h, 0100h, 0d9fch ;no operands

;----- FRSTOR

Op169 Dw 0210h, 0000h, 0000h, 0122h, 00ddh ;mem/addr

;----- FSAVE

Op171 Dw 0210h, 0000h, 0000h, 0132h, 00ddh ;mem/addr

;----- FSCALE

Op172 Dw 0000h, 0000h, 0000h, 0100h, 0d9fdh ;no operands

;----- FSQRT

Op173 Dw 0000h, 0000h, 0000h, 0100h, 0d9fah ;no operands

;----- FST

Op174 Dw 0001h, 0001h, 0000h, 0100h, 0ddd1h ;none,none
 Dw 0400h, 0001h, 0000h, 0112h, 00ddh ;ST(i),none
 Dw 0400h, 0800h, 0000h, 0112h, 00ddh ;ST(i),ST(0)
 Dw 0210h, 0801h, 0004h, 0152h, 00d9h ;mem/addr,ST(0)/none
 Dw 0210h, 0801h, 0008h, 0152h, 00ddh ;mem/addr,ST(0)/none

;----- FIST

Op175 Dw 0210h, 0801h, 0002h, 0152h, 00dfh ;mem/addr,ST(0)/none
 Dw 0210h, 0801h, 0004h, 0152h, 00dbh ;mem/addr,ST(0)/none

;----- FSTCW

Op176 Dw 0210h, 0000h, 0002h, 017ah, 00d9h ;mem/addr

;----- FSTENV

Op177 Dw 0210h, 0000h, 0000h, 0132h, 00d9h ;mem/addr

;----- FSTP

Op178 Dw 0001h, 0001h, 0000h, 0100h, 0ddd9h ;none,none
 Dw 0400h, 0001h, 0000h, 011ah, 00ddh ;ST(i),none
 Dw 0400h, 0800h, 0000h, 011ah, 00ddh ;ST(i),ST(0)
 Dw 0210h, 0801h, 0004h, 015ah, 00d9h ;mem/addr,ST(0)/none
 Dw 0210h, 0801h, 0008h, 015ah, 00ddh ;mem/addr,ST(0)/none
 Dw 0210h, 0801h, 0010h, 017ah, 00dbh ;mem/addr,ST(0)/none

;----- FISTP

Op179 Dw 0210h, 0801h, 0002h, 015ah, 00dfh ;mem/addr,ST(0)/none
 Dw 0210h, 0801h, 0004h, 015ah, 00dbh ;mem/addr,ST(0)/none
 Dw 0210h, 0801h, 0008h, 017ah, 00dfh ;mem/addr,ST(0)/none

;----- FBSTP

Op180 Dw 0210h, 0801h, 0010h, 0172h, 00dfh ;mem/addr,ST(0)/none

;----- FSTSW

Op181 Dw 0210h, 0000h, 0002h, 017ah, 00ddh ;mem/addr

;----- FTST

Op182 Dw 0000h, 0000h, 0000h, 0100h, 0d9e4h ;no operands

;----- FWAIT

Op183 Dw 0000h, 0000h, 0000h, 0000h, 009bh ;no operands

;----- FXAM

Op184 Dw 0000h, 0000h, 0000h, 0100h, 0d9e5h ;no operands

;----- FXCH

Op185 Dw 0001h, 0001h, 0000h, 0100h, 0d9c9h ;none,none
 Dw 0400h, 0001h, 0000h, 010ah, 00d9h ;ST(i),none
 Dw 0800h, 0400h, 0000h, 110ah, 00d9h ;ST(0),ST(i)
 Dw 0400h, 0800h, 0000h, 010ah, 00d9h ;ST(i),ST(0)

;----- FXTRACT

Op186 Dw 0000h, 0000h, 0000h, 0100h, 0d9f4h ;no operands

;----- FYL2X

Op170 Dw 0000h, 0000h, 0000h, 0100h, 0d9f1h ;no operands

;----- FYL2XP1

Op187 Dw 0000h, 0000h, 0000h, 0100h, 0d9f9h ;no operands
