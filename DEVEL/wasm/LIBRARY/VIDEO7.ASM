;********************************;
; WASM Video Module, Screen Copy ;
; By Eric Tauck                  ;
;                                ;
; Defines:                       ;
;                                ;
;   ScrCpy  copy a screen area   ;
;                                ;
; Requires:                      ;
;                                ;
;   VIDEO1.ASM                   ;
;********************************;

        jmp     _video7_end

;========================================
; Copy a screen rectangle to another
; part of the screen.
;
; In: BH= top left row; BL= top left
;     column; CH= bottom left row; CL=
;     bottom left column; AH= new row;
;     AL= new column.

ScrCpy  PROC    NEAR
        push    di
        push    si
        push    ds
        push    es
        mov     dx, ax          ;new location in DH and DL

;--- determine direction

        cmp     bh, dh
        ja      _srcpy2
        jb      _srcpy1
        cmp     bl, dl
        ja      _srcpy2
        je      _srcpy5

;--- reverse copy

_srcpy1 mov     al, ch          ;bottom row
        mul     _vid_cols       ;times columns per row
        add     al, cl          ;add column
        adc     ah, 0           ;make 16 bit add
        mov     si, ax          ;source offset

        mov     al, ch          ;bottom
        sub     al, bh          ;minus top
        add     al, dh          ;add new top to get new bottom
        mul     _vid_cols       ;get address
        mov     di, ax          ;destination offset
        mov     al, cl          ;right
        sub     al, bl          ;minus left
        add     al, dl          ;add new left to get new right
        sub     ah, ah          ;zero high byte
        add     di, ax          ;add to offset

        mov     al, _vid_cols   ;load columns
        sub     ah, ah          ;zero high byte
        neg     ax              ;negate
        std                     ;copy in reverse direction
        jmps    _srcpy3

;--- forward copy

_srcpy2 mov     al, bh          ;row
        mul     _vid_cols       ;times column per row
        add     al, bl          ;add column
        adc     ah, 0           ;make 16 bit add
        mov     si, ax          ;source offset

        mov     al, dh          ;new row
        mul     _vid_cols       ;times columns per row
        add     al, dl          ;add column
        adc     ah, 0           ;16 bit add
        mov     di, ax          ;destination offset

        mov     al, _vid_cols   ;load columns
        sub     ah, ah          ;zero high byte
        cld                     ;copy in forward direction

;--- finish setting up registers

_srcpy3 sub     ch, bh          ;subtract bottom from top
        inc     ch              ;rows to copy
        mov     dl, ch          ;DL has rows to copy

        sub     cl, bl          ;subtract left from right
        inc     cl              ;columns to copy
        sub     ch, ch          ;CX has columns to copy

        shl     di              ;destination offset times two
        add     di, _vid_base   ;add base
        mov     es, _vid_addrseg ;load destination segment
        shl     si              ;source offset times two
        add     si, _vid_base   ;add base
        push    es
        pop     ds              ;load source segment

        shl     ax              ;offset from row to row times two

;--- loop for each row

_srcpy4 push    cx
        push    di
        push    si
        rep
        movsw                   ;copy a row
        pop     si
        pop     di
        pop     cx
        add     di, ax          ;next destination row
        add     si, ax          ;next source row
        dec     dl              ;decrement count
        jnz     _srcpy4         ;loop back if more

;--- finished

_srcpy5 pop     es
        pop     ds
        pop     si
        pop     di
        ret
        ENDP

_video7_end
