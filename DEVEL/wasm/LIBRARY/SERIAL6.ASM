;**********************************;
; WASM Serial I/O, Empty Pipe      ;
; By Eric Tauck                    ;
;                                  ;
; Defines:                         ;
;                                  ;
;   ComEmp  wait for pause in flow ;
;                                  ;
; Requires:                        ;
;                                  ;
;   SERIAL1.ASM                    ;
;   TICKS.ASM                      ;
;**********************************;

        jmps    _serial6_end

;========================================
; Empty the pipe (wait for a pause in the
; data flow).
;
; In: BX= record address; AX= pause
;     duration ticks; DX= timeout ticks.
;
; Out: CY= set if timeout (i.e. no
;      pause).

ComEmp  PROC    NEAR
        push    di
        push    si
        push    bp

        mov     di, ax
        mov     si, dx
        mov     bp, bx

;--- start outer timer

        sub     al, al          ;timer zero
        call    TicRes          ;reset timer

;--- start/restart inner timer

_cmemp1 mov     al, 1           ;timer 1
        call    TicRes          ;reset timer
        mov     bx, bp
        call    ComClr          ;clear any input

;--- check if timeout

_cmemp2 sub     al, al          ;timer zero
        call    TicPas          ;get ticks
        cmp     ax, si          ;check if timeout
        jae     _cmemp3         ;exit if so

;--- check if any bytes received

        mov     bx, bp
        call    ComByt          ;get bytes in buffer
        or      ax, ax          ;check if any
        jnz     _cmemp1         ;restart timer if so

;--- check if inner timeout

        mov     al, 1           ;timer 1
        call    TicPas          ;get ticks passed
        cmp     ax, di          ;check if timeout
        jb      _cmemp2         ;loop back if

;--- finished with pause

        pop     bp
        pop     si
        pop     di
        clc
        ret

;--- finished by timeout (no pause in flow)

_cmemp3 pop     bp
        pop     si
        pop     di
        stc
        ret
        ENDP

_serial6_end
