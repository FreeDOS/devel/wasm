;*************************************;
; WASM Video Module, Screen Routines  ;
; By Eric Tauck                       ;
;                                     ;
; Defines:                            ;
;                                     ;
;   ScrCls  clear the whole screen    ;
;   ScrClr  clear a window            ;
;   ScrPut  copy a window to memory   ;
;   ScrGet  copy memory to a window   ;
;   ScrSiz  bytes required for window ;
;                                     ;
; Requires:                           ;
;                                     ;
;   VIDEO1.ASM                        ;
;*************************************;

        jmp     _video6_end

;========================================
; Local routine to the screen
; coordinates.
;
; In: BH= top left row; BL= top left
;     column; CH= bottom left row; CL=
;     bottom left column.
;
; Out: AX= starting screen offset; CX=
;      columns; BL= rows; DX= offset
;      from start of row to start of
;      next row.

_vid_scraddr PROC NEAR
        mov     dl, _vid_cols   ;columns per row

;--- determine starting address

        mov     al, bh          ;starting row
        mul     al, dl          ;times columns per row
        add     al, bl          ;add column
        adc     ah, 0           ;16 bit add
        shl     ax              ;times two (for attribute/character)
        add     ax, _vid_base   ;add base address

;--- determine rows to copy

        sub     ch, bh          ;determine rows
        inc     ch              ;adjust, put in BL later

;--- determine columns

        sub     cl, bl          ;determine columns to copy
        inc     cl              ;adjust, clear CH later

;--- adjust values

        mov     bl, ch          ;row count in BL
        sub     ch, ch          ;column count in CX
        sub     dh, dh          ;zero high byte
        shl     dx              ;offset from row to row
        ret
        ENDP

;========================================
; Clear the whole screen.

ScrCls  PROC    NEAR
        sub     bx, bx          ;0, 0 is upper left
        mov     cl, _vid_cols
        dec     cl              ;lower left column
        mov     ch, _vid_rows
        dec     ch              ;lower left row
        call    ScrClr          ;clear screen
        ret
        ENDP

;========================================
; Clear part of the screen.
;
; In: BH= top left row; BL= top left
;     column; CH= bottom left row; CL=
;     bottom left column.

ScrClr  PROC    NEAR
        push    di
        push    es

;--- set up registers

        call    _vid_scraddr    ;set up registers
        mov     di, ax          ;screen offset
        mov     es, _vid_addrseg ;screen segment

;--- blank each row

        cld
        mov     ah, _vid_attr   ;load attribute
        mov     al, ' '         ;space for blanking

_srclr1 push    cx
        push    di
        rep
        stosw                   ;blank a row
        pop     di
        pop     cx
        add     di, dx          ;offset to next row
        dec     bl              ;decrement row count
        jnz     _srclr1         ;loop back if more rows

        pop     es
        pop     di
        ret
        ENDP

;========================================
; Write part of the screen to memory.
;
; In: BH= top left row; BL= top left
;     column; CH= bottom left row; CL=
;     bottom left column; DX:AX=
;     destination address.

ScrGet  PROC    NEAR
        push    di
        push    si
        push    ds
        push    es

;--- set up registers

        push    dx
        push    ax
        call    _vid_scraddr    ;set up registers
        mov     si, ax          ;save offset
        mov     ds, _vid_addrseg ;load screen segment
        pop     di              ;
        pop     es              ;load destination address

;--- copy data to memory

        cld

_srget1 push    cx
        push    si
        rep
        movsw                   ;copy a row to the memory
        pop     si
        pop     cx
        add     si, dx          ;offset to next row
        dec     bl              ;decrement row count
        jnz     _srget1         ;loop back if more rows

        pop     es
        pop     ds
        pop     si
        pop     di
        ret
        ENDP

;========================================
; Write memory to the screen.
;
; In: BH= top left row; BL= top left
;     column; CH= bottom left row; CL=
;     bottom left column; DX:AX= source
;     address.

ScrPut  PROC    NEAR
        push    di
        push    si
        push    ds
        push    es

;--- set up registers

        push    dx
        push    ax
        call    _vid_scraddr    ;set up registers
        mov     di, ax          ;save offset
        mov     es, _vid_addrseg ;load screen segment
        pop     si              ;
        pop     ds              ;load source address

;--- copy data to screen

        cld

_srput1 push    cx
        push    di
        rep
        movsw                   ;copy a row to the screen
        pop     di
        pop     cx
        add     di, dx          ;offset to next row
        dec     bl              ;decrement row count
        jnz     _srput1         ;loop back if more rows

        pop     es
        pop     ds
        pop     si
        pop     di
        ret
        ENDP

;========================================
; Determine the bytes required to store
; part of the screen.
;
; In: BH= top left row; BL= top left
;     column; CH= bottom left row; CL=
;     bottom left column.
;
; Out: AX= bytes required.

ScrSiz  PROC    NEAR
        mov     al, cl
        sub     al, bl  ;columns
        inc     al
        sub     ch, bh  ;rows
        inc     ch
        mul     al, ch  ;total cells
        shl     ax      ;times two, total bytes
        ret
        ENDP

_video6_end
