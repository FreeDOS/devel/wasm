;********************************;
; WASM Extended Error Module     ;
; By Eric Tauck                  ;
;                                ;
; Defines:                       ;
;                                ;
;   ErrSet  save next error data ;
;   ErrGet  get last error data  ;
;********************************;

;--- hook critical error handler

        mov     ax, 2524H
        mov     dx, OFFSET _ErrInt
        int     21H
        jmps    _error2_end

;--- data

_error1 DW      0               ;number
_error2 DW      ?               ;class and suggested action
_error3 DB      ?               ;locus

;================================================
; Critical error handler.

_ErrInt PROC    FAR
        push    bx
        push    cx
        push    dx
        push    ds
        push    cs
        pop     ds              ;load DS
        call    ErrSys          ;get system error
        call    ErrSet          ;save the error
        pop     ds
        pop     dx
        pop     cx
        pop     bx
        mov     al, 3
        iret
        ENDP

;================================================
; Set the error data.
;
; Out: AX= error; BH= class; BL= action; CH=
;      locus.

ErrSet  PROC    NEAR
        mov     _error1, ax             ;save error
        mov     _error2, bx             ;save class and action
        mov     _error3, ch             ;save locus
        ret
        ENDP

;================================================
; Get the error data.
;
; Out: AX= error; BH= class; BL= action; CH=
;      locus.

ErrGet  PROC    NEAR
        mov     ax, _error1             ;load error
        mov     bx, _error2             ;load class and action
        mov     ch, _error3             ;load locus
        mov     _error1, 0              ;zero error
        or      ax, ax                  ;check if defined
        jnz     _erget1
        call    ErrSys                  ;get error direct from system
_erget1 ret
        ENDP

_error2_end
