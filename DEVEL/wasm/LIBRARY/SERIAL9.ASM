;********************************;
; WASM Serial I/O, Replacement   ;
; By Eric Tauck                  ;
;                                ;
; Defines:                       ;
;                                ;
;   ComRep  replace a byte       ;
;                                ;
; Requires:                      ;
;                                ;
;   SERIAL1.ASM                  ;
;********************************;

        jmps    _serial9_end

;========================================
; Replace an input byte.
;
; In: AL= byte; BX= serial record.

ComRep  PROC    NEAR
        push    di
        push    es

;--- store byte

        les     di, [bx + _COM_BUFPTR2] ;buffer read address
        cmp     di, [bx + _COM_BUFBEG]  ;check if at beginning
        jne     _cmrep1
        mov     di, [bx + _COM_BUFEND]  ;wrap to end
_cmrep1 dec     di                      ;decrement pointer
        seg     es
        mov     [di], al                ;save byte
        mov     [bx + _COM_BUFPTR2], di ;save pointer

;--- update bytes in buffer

        mov     ax, [bx + _COM_BUFBYTE] ;load byte count
        cmp     ax, [bx + _COM_BUFSIZE] ;compare to size
        je      _cmrep2                 ;skip increment if full
        inc     WORD [bx+_COM_BUFBYTE]  ;increment bytes

_cmrep2 pop     es
        pop     di
        ret
        ENDP

_serial9_end
