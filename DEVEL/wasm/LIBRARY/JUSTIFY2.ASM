;**********************************;
; WASM String Left Justify         ;
; By Eric Tauck                    ;
;                                  ;
; Defines:                         ;
;                                  ;
;   StrJusL  left justify a string ;
;                                  ;
; Requires:                        ;
;                                  ;
;    STRING.ASM                    ;
;**********************************;

        jmps    _justify2_end

;========================================
; Left justify a string.
;
; In: AX= string; CX= final length of
;     string; DL= pad character.

StrJusL PROC    NEAR
        push    di

        push    cx
        push    dx
        mov     di, ax
        call    StrLen          ;get length
        pop     dx
        pop     cx
        sub     cx, ax          ;characters to pad
        jbe     _stjul1         ;exit if no padding

        add     di, ax          ;point to last byte
        mov     al, dl
        cld
        rep
        stosb                   ;store padding
        sub     al, al
        stosb                   ;store new NUL

_stjul1 pop     di
        ret
        ENDP

_justify2_end
