;*****************************************;
; WASM Start-Up Module                    ;
; By Eric Tauck                           ;
;                                         ;
; This code verifies that DOS version 3   ;
; or higher is running and reduces the    ;
; memory allocation to a minimum. The     ;
; default stack size may be modified by   ;
; changing the STACK_SIZE equate below.   ;
;*****************************************;

STACK_SIZE      EQU     4096    ;bytes reserved for stack, can be changed

;--- check DOS version

        mov     ah, 30H         ;get OS version function
        int     21H             ;execute
        cmp     al, 3           ;check if at least version 3
        jb      _sartu1         ;jump if not

;--- set up memory allocation

        mov     ax, $END        ;load offset of end of program
        add     ax, STACK_SIZE  ;add space for stack
        mov     cl, 4
        shr     ax, cl          ;convert to paragraph form
        inc     ax              ;round up
        mov     bx, ax          ;save paragraphs
        shl     ax, cl
        cmp     ax, sp          ;check if out of memory already
        ja      _sartu2         ;exit if so
        mov     sp, ax          ;set up new stack

;--- reduce memory allocation

        mov     ah, 4AH         ;modify memory allocation function
        int     21H             ;execute
        jc      _sartu2         ;jump if error

        jmps    _start_end

;--- wrong DOS version

_sartu1 mov     ah, 9                   ;display message function
        mov     dx, OFFSET _stup_mes1   ;address of message
        int     21H                     ;execute
        int     20H                     ;terminate

;--- not enough memory

_sartu2 mov     ah, 9                   ;display message function
        mov     dx, OFFSET _stup_mes2   ;address of message
        int     21H                     ;execute
        mov     ax, 4CFFH               ;exit with error code
        int     21H                     ;execute

;--- data

_stup_mes1      DB      'Requires DOS 3.0',13,10,'$'
_stup_mes2      DB      'Not enough memory',13,10,'$'

_start_end
