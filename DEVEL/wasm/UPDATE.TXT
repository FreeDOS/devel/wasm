

                    Wolfware Assembler Instruction Reference

       The following is a summary of all legal WASM directives and
       instructions.  The first table lists the instructions by category.
       The second table lists the instructions alphabetically and shows
       their syntax.

                             Instruction Categories

       Directives
       ----------

       DB              EXPAND+         LABEL           PAGE-
       DS              EXPAND-         LINESIZE        PAGESIZE
       DW              ERRORMAX        LIST+           PROC
       ELSE            FLAGALL+        LIST-           RESETC
       ELSEIF          FLAGALL-        MACRO           SUBTITLE
       ENDIF           IF              MACROC          SYMDUMP+
       ENDM            IFN             NEXTIF          SYMDUMP-
       ENDP            INCLUDE         ORG             TITLE
       EQU             JUMP+           PAGE            UNUSED+
       ERROR           JUMP-           PAGE+           UNUSED-

       Data Transfer
       -------------

       IN              LEA             OUTSW           PUSHA
       INSB            LES             POP             PUSHF
       INSW            MOV             POPA            SAHF
       LAHF            OUT             POPF            XCHG
       LDS             OUTSB           PUSH            XLAT

       Arithmetic
       ----------

       AAA             ADD             DEC             MUL
       AAD             CBW             DIV             NEG
       AAM             CWD             IDIV            SBB
       AAS             DAA             IMUL            SUB
       ADC             DAS             INC

       Logic
       -----

       AND             RCL             ROR             SHL
       NOT             RCR             SAL             SHR
       OR              ROL             SAR             XOR

       Flag Setting
       ------------

       CLC             CLTS            STC             TEST
       CLD             CMC             STD
       CLI             CMP             STI


       Control Transfer
       ----------------

       BOUND           JE              JNG             JZ
       CALL            JG              JNGE            LEAVE
       ENTER           JGE             JNL             LOOP
       INT             JL              JNLE            LOOPE
       INT3            JLE             JNO             LOOPNE
       INTO            JMP             JNP             LOOPNZ
       IRET            JMPS            JNS             LOOPZ
       JA              JNA             JNZ             RET
       JAE             JNAE            JO              RETF
       JB              JNB             JP              RETN
       JBE             JNBE            JPE
       JC              JNC             JPO
       JCXZ            JNE             JS

       String Manipulation
       -------------------

       CMPSB           LODSW           REP             SCASB
       CMPSW           MOVSB           REPE            SCASW
       INSB            MOVSW           REPNE           STOSB
       INSW            OUTSB           REPNZ           STOSW
       LODSB           OUTSW           REPZ

       Processor Control
       -----------------

       ARPL            LGDT            LTR             SMSW
       CS:             LIDT            NOP             SS:
       DS:             LLDT            SEG             STR
       ES:             LMSW            SGDT            VERR
       HLT             LOCK            SIDT            VERW
       LAR             LSL             SLDT            WAIT

       8087 Data Transfer
       ------------------

       FLD             FST             FISTP
       FILD            FIST            FBSTP
       FBLD            FSTP            FXCH

       8087 Arithmetic
       ---------------

       FADD            FDIVP           FPREM           FSUBR
       FIADD           FDIVR           FRNDINT         FISUBR
       FADDP           FIDIVR          FSCALE          FSUBRP
       FABS            FDIVRP          FSQRT           FXTRACT
       FCHS            FMUL            FSUB
       FDIV            FIMUL           FISUB
       FIDIV           FMULP           FSUBP


       8087 Transcendental
       -------------------

       F2XM1           FPTAN           FYL2XP1
       FPATAN          FYL2X

       8087 Constants
       --------------

       FLD1            FLDL2T          FLDLN2          FLDZ
       FLDL2E          FLDLG2          FLDPI

       8087 Flag Setting
       -----------------

       FCOM            FCOMP           FCOMPP          FXAM
       FICOM           FICOMP          FTST

       8087 Processor Control
       ----------------------

       FCLEX           FINIT           FNINIT          FNSTSW
       FDECSTP         FLDCW           FNOP            FSAVE
       FDISI           FLDENV          FNSAVE          FSTCW
       FENI            FNCLEX          FNSTCW          FSTENV
       FFREE           FNDISI          FNSTENV         FSTSW
       FINCSTP         FNENI           FRSTOR          FWAIT


                               Instruction Syntax

       Syntax Operand Types:

         reg   register
         seg   segment register
         mem   memory reference
         imed  immediate data
         str   string
         stk   8087 stack

       Numbers following the type specify the size of operand in bits.
       Operands separated by slashes mean that either one may be used.
       Operands in capital letters are the literal operands accepted.
       Operands in brackets are optional, all others are required.
       Default (often abbreviated "def") indicates a default setting.
       Instructions followed by a 186 will work only on a 80186
       processor.  Instructions followed by a 286 will work only on a
       80286 processor.

       AAA                             CALL n
         ASCII adjust for add          CALL imed, imed
                                       CALL reg16/mem16
       AAD                             CALL mem32
         ASCII adjust for divide         Call

       AAM                             CBW
         ASCII adjust for multiply       Convert byte to word

       AAS                             CLC
         ASCII adjust for subtract       Clear carry

       ADC reg/mem, imed               CLD
       ADC reg/mem, reg                  Clear direction
       ADC reg, reg/mem
         Add with carry                CLI
                                         Clear interrupt
       ADD reg/mem, imed
       ADD reg/mem, reg                CLTS
       ADD reg, reg/mem                  Clear task switchd flag, 286
         Add
                                       CMC
       AND reg/mem, imed                 Clear complement carry
       AND reg/mem, reg
       AND reg, reg/mem                CMP reg/mem, imed
         Logical and                   CMP reg/mem, reg
                                       CMP reg, reg/mem
       ARPL mem16/reg16, reg             Compare
         Adjust req priv level, 286
                                       CMPSB
       BOUND reg16, mem32                Compare string byte
         Check array bounds, 186/286


       CMPSW                           ERRORMAX imed
         Compare string word             Max errors, default 65535

       CS:                             ES:
         Code segment override           Extra segment override

       CWD                             EXPAND+
         Convert word to double word     Enable macro listing, def

       DAA                             EXPAND-
         Decimal adjust for add          Disable macro listing

       DAS                             F2XM1
         Decimal adjust for subtract     [2^ST]-1

       DB imed/str [,imed/str]...      FABS
         Declare bytes                   Absolute value of ST

       DEC reg/mem                     FADD
         Decrement                     FADD stk, ST
                                       FADD [ST,] stk
       DIV [acum,] reg/mem             FADD [ST,] mem32/mem64
         Unsigned divide                 Add real to ST

       DS imed [,imed8]                FADDP
         Declare storage               FADDP stk [,ST]
                                         Add ST to real and pop
       DS:
         Data segment override         FBLD [ST,] mem80
                                         Load BCD to ST
       DW imed [,imed]...
         Declare words                 FBSTP mem80 [,ST]
                                         Store ST to BCD and pop
       ELSE imed
         Conditional "else"            FCHS
                                         Change sign of ST
       ELSEIF imed
         Conditional "else-if"         FCLEX
                                         Clear exceptions
       ENDIF
         End conditional statements    FCOM
                                       FCOM [ST,] stk
       ENDM                            FCOM [ST,] mem32/mem64
         End of macro declaration        Compare real to ST

       ENDP                            FCOMP
         End of procedure              FCOMP [ST,] stk
                                       FCOMP [ST,] mem32/mem64
       ENTER                             Compare real to ST and pop
         Make stack frame, 186/286
                                       FCOMPP
       symbol EQU imed                   Comp ST(1) & ST, pop twice
         Equate symbol to value
                                       FDECSTP
       ERROR str                         Decrement stack pointer
         Programmer defined error


       FDISI                           FIST mem32/mem64 [,ST]
         Disable interrupts              Store ST to integer

       FDIV                            FISTP mem16/mem32/mem64 [,ST]
       FDIV stk, ST                      Store ST to integer and pop
       FDIV [ST,] stk
       FDIV [ST,] mem32/mem64          FISUB [ST,] mem16/mem32
         Divide ST by real               Subtract integer from ST

       FDIVP                           FISUBR [ST,] mem16/mem32
       FDIVP stk [,ST]                   Subtract ST from integer
         Divide ST by real and pop
                                       FLAGALL+
       FDIVR                             Flag all undefined symbols
       FDIVR stk, ST
       FDIVR [ST,] stk                 FLAGALL-
       FDIVR [ST,] mem32/mem64           Flag undef sym once, default
         Divide real by ST
                                       FLD
       FDIVRP                          FLD [ST,] stk
       FDIVRP stk [,ST]                FLD [ST,] mem32/mem64/mem80
         Divide real by ST and pop       Load to real to ST

       FENI                            FLD1
         Enable interrupts               Load 1.0 into ST

       FFREE                           FLDCW mem16
       FFREE stk                         Load control word
         Free stack entry
                                       FLDENV mem
       FIADD [ST,] mem16/mem32           Load environment
         Add integer to ST
                                       FLDL2E
       FICOM [ST,] mem16/mem32           Load log base 2 of e to ST
         Compare integer to ST
                                       FLDL2T
       FICOMP [ST,] mem16/mem32          Load log base 2 of 10 to ST
         Comp integer to ST and pop
                                       FLDLG2
       FIDIV [ST,] mem16/mem32           Load log base 10 of 2 to ST
         Divide ST by integer
                                       FLDLN2
       FIDIVR [ST,] mem16/mem32          Load log base e of 2 to ST
         Divide integer by ST
                                       FLDPI
       FILD [ST,] mem16/mem32/mem64      Load pi into ST
         Load integer to ST
                                       FLDZ
       FIMUL [ST,] mem16/mem32           Load 0 into ST
         Multiply integer with ST
                                       FMUL
       FINCSTP                         FMUL stk, ST
         Increment stack pointer       FMUL [ST,] stk
                                       FMUL [ST,] mem32/mem64
       FINIT                             Multiply real with ST
         Initialize 8087


       FMULP                           FST
       FMULP stk [,ST]                 FST stk [,ST] G
         Multiply real and ST, pop     FST mem32/mem64 [,ST]
                                         Store ST to real
       FNCLEX
         Clear exceptions, no wait     FSTCW mem16
                                         Store control word
       FNDISI
         Disable interrupts, no wait   FSTENV mem
                                         Store environment
       FNENI
         Enable interrupts, no wait    FSTP
                                       FSTP stk [,ST]
       FNINIT                          FSTP mem32/mem64/mem80 [,ST]
         Initialize 8087, no wait        Store ST to real and pop

       FNOP                            FSTSW mem16
         No operation                    Store status word

       FNSAVE mem                      FSUB
         Save state, no wait           FSUB stk, ST
                                       FSUB [ST,] stk
       FNSTCW mem16                    FSUB [ST,] mem32/mem64
         Store control word, no wait     Subtract real from ST

       FNSTENV mem                     FSUBP
         Store environment, no wait    FSUBP stk [,ST]
                                         Subtract real from ST, pop
       FNSTSW mem16
         Store status word, no wait    FSUBR
                                       FSUBR stk, ST
       FPATAN                          FSUBR [ST,] stk
         Partial arctan of ST / ST(1)  FSUBR [ST,] mem32/mem64
                                         Subtract ST from real
       FPREM
         Partial rem of ST / ST(1)     FSUBRP
                                       FSUBRP stk [,ST]
       FPTAN                             Subtract ST from real, pop
         Partial tangent of ST
                                       FTST
       FRNDINT                           Test ST
         Round ST to integer
                                       FWAIT
       FRSTOR mem                        Wait
         Restore state
                                       FXAM
       FSAVE mem                         Examine ST
         Save state
                                       FXCH
       FSCALE                          FXCH stk [,ST]
         Scale ST by ST(1)               Exchange

       FSQRT                           FXTRACT
         Square root of ST               Extract components of ST


       FYL2X                           JAE n
         ST(1)*[LOG2[ST]]                Jump if above or equal

       FYL2XP1                         JB n
         ST(1)*[LOG2[ST+1]]              Jump if below

       HLT                             JBE n
         Halt                            Jump if below or equal

       IDIV [acum,] reg/mem            JC n
         Signed divide                   Jump if carry

       IF imed                         JCXZ n
         Conditional "if"                Jump if CX equal to zero

       IFN imed                        JE n
         Conditional "if not"            Jump if equal

       IMUL [acum,] reg/mem            JG n
         Signed multiply                 Jump if greater

       IMUL reg16, imed                JGE n
       IMUL reg16, reg16, imed           Jump if greater or equal
         Signed multiply, 186/286
                                       JL n
       IN acum, imed8                    Jump if less
       IN acum, DX
         Input from port               JLE n
                                         Jump if less or equal
       INC reg/mem
         Increment                     JMP n
                                       JMP imed, imed
       INCLUDE str                     JMP reg16/mem16
         Include source file           JMP mem32
                                         Unconditional jump
       INSB
         Input string byte, 186/286    JMPS n
                                         Unconditional short jump
       INSW
         Input string word, 186/286    JNA n
                                         Jump if not above
       INT imed8
         Interrupt                     JNAE n
                                         Jump if not above or equal
       INT3
         Type 3 interrupt              JNB n
                                         Jump if not below
       INTO
         Interrupt on overflow         JNBE n
                                         Jump if not below or equal
       IRET
         Interrupt return              JNC n
                                         Jump if no carry
       JA n
         Jump if above                 JNE n
                                         Jump if not equal

       JNG n                           LAR reg16, reg16/mem16
         Jump if not greater             Load access rights, 286

       JNGE n                          LDS reg16, mem
         Jump if not greater or equal    Load DS and register

       JNL n                           LEA reg16, mem
         Jump if not less                Load effective address

       JNLE n                          LEAVE
         Jump if not less or equal       Reverse an ENTER, 186/286

       JNO n                           LES reg16, mem
         Jump if not overflow            Load ES and register

       JNP n                           LGDT mem64
         Jump if not parity              Load global descrip tab, 286

       JNS n                           LIDT mem64
         Jump if not sign                Load int descrip tab, 286

       JNZ n                           LINESIZE [imed] [,imed8] ...
         Jump if not zero                Set page width, default 79

       JO n                            LIST+
         Jump if overflow                Generate listing, default

       JP n                            LIST-
         Jump if parity                  List output off

       JPE n                           LLDT reg16/mem16
         Jump if parity even             Load local descrip tab, 286

       JPO n                           LMSW reg16/mem16
         Jump if parity odd              Load machine stat word, 286

       JS n                            LOCK
         Jump if sign                    Bus lock prefix

       JUMP+                           LODSB
         Flag short-long JMPs            Load string byte

       JUMP-                           LODSW
         Accept short-long JMPs, def     Load string word

       JZ n                            LOOP n
         Jump if zero                    Loop CX times

       symbol LABEL NEAR/FAR           LOOPE n
       symbol LABEL BYTE/WORD/DWORD      Loop while equal
       symbol LABEL DWORD/QWORD
       symbol LABEL TBYTE/ANYSIZE      LOOPNE n
         Define label                    Loop while not equal

       LAHF                            LOOPNZ n
         Load AH with flags              Loop while not zero


       LOOPZ n                         OUTSW
         Loop while zero                 Output string word, 186/286

       LSL reg16, reg16/mem16          PAGE [imed/str]
         Load segment limit, 286       PAGE imed [,str]
                                         Start new page
       LTR reg16/mem16
         Load task register, 286       PAGE+ [imed/str]
                                       PAGE+ imed [,str]
       symbol MACRO [symbol] ...         Start auto paging
         Start macro declaration
                                       PAGE-
       symbol MACROC [symbol] ...        Stop auto paging, default
         Start conditional macro
                                       PAGESIZE imed [,imed]
       MOV reg/mem, imed                 Page len & wid, def 60 & 79
       MOV reg/mem, reg/seg
       MOV reg/seg, reg/mem            POP reg/seg/mem
         Move                            Pop register or memory

       MOVSB                           POPA
         Move string byte                Pop all registers, 186/286

       MOVSW                           POPF
         Move string word                Pop flags

       MUL [acum,] reg/mem             PROC NEAR/FAR
         Unsigned multiply               Beginning of procedure

       NEG reg/mem                     PUSH reg/seg/mem
         Decrement                       Push register or memory

       NEXTIF imed                     PUSH imed
         Conditional "if", same level    Push immediate data, 186/286

       NOP                             PUSHA
         No operation                    Push all registers, 186/286

       NOT reg/mem                     PUSHF
         Logical not                     Push flags

       OR reg/mem, imed                RCL reg/mem
       OR reg/mem, reg                 RCL reg/mem, CL
       OR reg, reg/mem                   Rotate through carry left
         Logical or
                                       RCL reg/mem, imed8
       ORG imed                          Roll carry left, 186/286
         Origin, default 100H
                                       RCR reg/mem
       OUT imed8, acum                 RCR reg/mem, CL
       OUT DX, acum                      Rotate through carry right
         Output to a port
                                       RCR reg/mem, imed8
       OUTSB                             Roll carry right, 186/286
         Output string byte, 186/286


       REP                             SAR reg/mem, imed8
         Repeat CX times                 Shift arith right, 186/286

       REPE                            SBB reg/mem, imed
         Repeat while equal            SBB reg/mem, reg
                                       SBB reg, reg/mem
       REPNE                              Subtract with borrow
         Repeat while not equal
                                       SCASB
       REPNZ                             Scan string byte
         Repeat while not zero
                                       SCASW
       REPZ                              Scan string word
         Repeat while zero
                                       SEG seg
       RESETC [imed] [,imed]             Segment override prefix
         Reset counter & checksum
                                       SGDT mem64
       RET                               Store global dscrpt tab, 286
       RET imed
         Procedural return             SHL reg/mem
                                       SHL reg/mem, CL
       RETF                              Shift logical left
       RETF imed
         Far return                    SHL reg/mem, imed8
                                         Shift left, 186/286
       RETN
       RETN imed                       SHR reg/mem
         Near return                   SHR reg/mem, CL
                                         Shift logical right
       ROL reg/mem
       ROL reg/mem, CL                 SHR reg/mem, imed8
         Rotate left                     Shift right, 186/286

       ROL reg/mem, imed8              SIDT mem64
         Roll left, 186/286              Store intr dscrpt tab, 286

       ROR reg/mem                     SLDT reg16/mem16
       ROR reg/mem, CL                   Store local dscrpt tab, 286
         Rotate right
                                       SMSW reg16/mem16
       ROR reg/mem, imed8                Store machine stat word, 286
         Roll right, 186/286
                                       SS:
       SAHF                              Stack segment override
         Store AH to flags
                                       STC
       SAL reg/mem                       Set carry
       SAL reg/mem, CL
         Shift arithmetic left         STD
                                         Set direction
       SAL reg/mem, imed8
         Shift arith left, 186/286     STI
                                         Set interrupt
       SAR reg/mem
       SAR reg/mem, CL                 STOSB
         Shift arithmetic right          Store string byte

       STOSW
         Store string word

       STR reg16/mem16
         Store task register, 286

       SUB reg/mem, imed
       SUB reg/mem, reg
       SUB reg, reg/mem
         Subtract

       SUBTITLE str
         Set program subtitle

       SYMDUMP+
         Enable symbol table

       SYMDUMP-
         Disable symbol table, def

       TEST reg/mem, imed
       TEST reg/mem, reg
       TEST reg, reg/mem
         Test

       TITLE str [,str]
         Program title and subtitle

       UNUSED+
         Flag unused symbols

       UNUSED-
         Don't flag unused sym, def

       VERR reg16/mem16
         Verify read, 286

       VERW reg16/mem16
         Verify write, 286

       WAIT
         Wait

       XCHG reg/mem, reg
       XCHG reg, reg/mem
         Exchange

       XLAT
         Translate byte in AL

       XOR reg/mem, imed
       XOR reg/mem, reg
       XOR reg, reg/mem
         Logical xor
